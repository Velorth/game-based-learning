﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CourseMaster
{
    class Element
    {
        String text = "";
        Boolean newline = false;
        int tabsCount = 0;
        Boolean correct = true;
        int spaceCount = 0;
        int id = 0;
        static int counter = 0;
		int score = 1;

        public Element()
        {
            ++counter;
            id = Element.counter;
        }

		public static List<TreeNode> ParseText(String save)
		{
			counter = 0;
			String[] lines = save.Split(new char[]{'\n'});
			List<TreeNode> result = new List<TreeNode>();
			TreeNode topicNode = null;
			TreeNode taskNode = null;
			TreeNode elementNode = null;
			Dictionary<int, TreeNode> elementNodes = new Dictionary<int,TreeNode>();
			foreach (String line in lines)
			{
				String[] words = line.Split(new char[] {' '});
				if (line.IndexOf("Element") == 0)
				{
					Element element = new Element();
					element.id = int.Parse(words[0].Substring(8));
					int parent = int.Parse(words[1].Substring(7));
					element.SpaceCount = int.Parse(words[2].Substring(7));
					element.TabsCount = int.Parse(words[3].Substring(5));
					element.NewLine = int.Parse(words[4].Substring(8)) == 1;
					element.correct = int.Parse(words[5].Substring(8)) == 1;
					element.AdditionalScore = int.Parse(words[6].Substring(6));
					element.Text = words[7].Substring(5);

					elementNode = new TreeNode(element.Text);
					elementNode.Tag = element;

					elementNodes[parent].Nodes.Add(elementNode);
					elementNodes[element.ID] = elementNode;

					counter = element.id > counter ? element.id : counter;

				}
                if (line.IndexOf("Before") == 0)
                {
                    Task task = taskNode.Tag as Task;
                    task.CodeBefore = line.Substring(7);
                }

				if (line.IndexOf("Task") == 0)
				{
					Task task = new Task();
					task.Description = line.Substring(5);

					taskNode = new TreeNode(task.Description);
					taskNode.Tag = task;

					elementNodes[0] = taskNode;
					topicNode.Nodes.Add(taskNode);
				}
				if (line.IndexOf("NextLevel") == 0)
				{
					Topic topic = new Topic();
					topic.NextLevel = int.Parse(words[0].Substring(10));
					topic.Name = line.Substring(words[0].Length+7);

					topicNode = new TreeNode(topic.Name);
					topicNode.Tag = topic;

					result.Add(topicNode);
				}
			}

			return result;
		}

		[Browsable(false)]
        public int ID
        {
            get { return id; }
        }

        #region Properties
		[DisplayName("Стоимость")]
		[Description("Стоимость определяет, сколько очков получит игрок за взятие лексемы. Для неправильных всегда 0")]
		public int AdditionalScore
		{
			get { return score; }
			set { score = value; }
		}
		[DisplayName("Пробелы")]
		[Description("Количество пробелов после элемента")]
        public int SpaceCount
        {
            get { return spaceCount; }
            set { spaceCount = value; }
        }
        public String Text
        {
            get { return text; }
            set { text = value; }
        }

		[DisplayName("С новой строки")]
		[Description("Будет ли писаться элемент с новой строки")]
        public Boolean NewLine
        {
            get { return newline; }
            set { newline = value; }
        }

		[DisplayName("Правильность")]
		[Description("Определяет, является ли элемент правильным продолжением цепочки")]
        public Boolean IsCorrect
        {
            get { return correct; }
            set { correct = value; }
        }

		[DisplayName("Тубуляции")]
		[Description("Количество символов табуляцции перед элементом")]
        public int TabsCount
        {
            get { return tabsCount; }
            set { tabsCount = value; }
        }
        #endregion
    }

    class Task
    {
        String description = "";
        String beforeCode = "";
		int stepSize = 3;

        #region Properties

        [DisplayName("Код до:")]
        [Description("Код, который будет отображен в листинге перед кодом игрока.")]
        public String CodeBefore
        {
            get { return beforeCode; }
            set { beforeCode = value; }
        }

		[DisplayName("Формулировка")]
		[Description("Формулировка задания. Строки разделяются при помощи символов \"\\n\"")]
        public String Description
        {
            get { return description; }
            set { description = value; }
        }

		[DisplayName("Размер шага")]
		[Description("Количество лексем, которые отображаются на поле")]
		public int StepSize
		{
			get
			{
				return stepSize;
			}
			set
			{
				stepSize = value;
			}
		}

        #endregion
    }

    class Topic
    {
		String name = "";
		int nextLevel = 1;

		#region Properties

		[DisplayName("До следующего")]
		[Description("Сколько заданий нужно решить, чтобы открыть следующий топик")]
		public int NextLevel
		{
			get { return nextLevel; }
			set { nextLevel = value; }
		}

		[DisplayName("Название")]
		[Description("Название топика")]
        public String Name
        {
            get { return name; }
            set { name = value; }
        }
        #endregion
    }
}