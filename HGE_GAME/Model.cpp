#include "Engine.h"


Model::Model()
{
}
Model::~Model()
{
}

	SpriteModel::SpriteModel()
	{
		mSprite = 0;
	}
	SpriteModel::SpriteModel(const char* texture, float x, float y, float w, float h)
	{
		HTEXTURE tex = Engine::GetSingleton()->GetResourceManager()->GetTexture(texture);
		mSprite = new hgeSprite(tex, x, y, w, h);
	}
	SpriteModel::SpriteModel(hgeSprite* sprite)
	{
		mSprite = sprite;
	}
	void SpriteModel::SetSprite(hgeSprite* sprite)
	{
		if (mSprite)
			delete mSprite;

		mSprite = sprite;
	}
	void SpriteModel::SetSprite(const char* texture, float x, float y, float w, float h)
	{
		if (mSprite)
			delete mSprite;

		HTEXTURE tex = Engine::GetSingleton()->GetResourceManager()->GetTexture(texture);
		mSprite = new hgeSprite(tex, x, y, w, h);
	}
	SpriteModel::~SpriteModel()
	{
	}

	float SpriteModel::GetSizeX() {return mSizeY;}
	float SpriteModel::GetSizeY() {return mSizeX;}
	void SpriteModel::SetSize(float sizex, float sizey) {mSizeX = sizey; mSizeY = sizex;}
	void SpriteModel::Render(float x, float y, float angle)
	{
		float mX = x;
		float mY = y;

		Vector2 v0, v1,v2,v3;
		v0.x = mX + 0.5f * (mSizeX * cos(angle) + mSizeY * sin(angle));
		v0.y = mY + 0.5f * (mSizeX * sin(angle) - mSizeY * cos(angle));
		v1.x = v0.x - mSizeY * sin(angle);
		v1.y = v0.y + mSizeY * cos(angle);
		v2.x = v1.x - mSizeX * cos(angle);
		v2.y = v1.y - mSizeX * sin(angle);
		v3.x = v0.x - mSizeX * cos(angle);
		v3.y = v0.y - mSizeX * sin(angle);
		mSprite->Render4V(v0.x, v0.y, v1.x, v1.y, v2.x, v2.y, v3.x, v3.y);
	}

	void SpriteModel::Update(float elapsedTime)
	{
		
	}