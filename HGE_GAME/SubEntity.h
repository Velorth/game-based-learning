#ifndef SUBENTITY_H
#define SUBENTITY_H

#include <list>

class SubEntity
{
public:
	SubEntity()
	{
		mParent = 0;
	}

	virtual ~SubEntity()
	{
	}

	virtual void Add(SubEntity* child)
	{
		mChildren.push_bcak(child);
	}


	virtual void Render(float x, float y, float angle)
	{

	}

	virtual void Update(float elapsedTime)
	{
	}
protected:
	// Members
	SubEntity* mParent;
	std::list<SubEntity*> mChildren;
	float mX;
	float mY;
	float mAngle;
	// Methods
	float GetNativeX() {return 0.0f;}
	float GetNativeY() {return 0.0f;}

	virtual void OnRender()
	{
	}
};

#endif