#ifndef SOUNDMANAGER_H
#define SOUNDMANAGER_H

#include "Engine.h"

class SoundManager
{
public:
	static SoundManager* GetSingleton();
	void SetUseSound(bool use);
	void SetBackGroundMusic(std::string file);
	void PlayEffect(HEFFECT effect);
	bool GetUseSound();
protected:
	SoundManager();
	virtual ~SoundManager();
	bool mUseSound;
	static SoundManager* mInstance;
	HEFFECT mMusic;
	HCHANNEL mMusicChanel;
};

#endif