#ifndef MODEL_H
#define MODEL_H

#include "Camera.h"
#include "Vector2.h"

class Model
{
public:
	Model();
	virtual ~Model();

	virtual void Render(float x, float y, float angle) = 0;

	virtual void Update(float elapsedTime) = 0;
protected:
private:
};

class SpriteModel : public Model
{
public:
	SpriteModel();
	SpriteModel(const char* texture, float x, float y, float w, float h);
	SpriteModel(hgeSprite* sprite);
	void SetSprite(hgeSprite* sprite);
	void SetSprite(const char* texture, float x, float y, float w, float h);
	virtual ~SpriteModel();

	float GetSizeX();
	float GetSizeY();
	void SetSize(float sizex, float sizey);
	virtual void Render(float x, float y, float angle);

	virtual void Update(float elapsedTime);
protected:
	hgeSprite* mSprite;
	float mSizeX;
	float mSizeY;
};
#endif
