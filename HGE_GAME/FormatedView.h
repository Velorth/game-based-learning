#ifndef FORMATED_VIEW_H
#define FORMATED_VIEW_H

#include <string>
#include <list>
#include "GUI.h"

class TextPart
{
public:
	std::string text;
	hgeFont*	font;
};

typedef std::list<TextPart> FormatedText;

class FormatedView : public GUIObject
{
public:
	FormatedView();
	virtual ~FormatedView();

	int GetMode();
	void SetMode(int align);
	hgeRect GetPosition();
	void SetPosition(float x, float y, float w, float h);
	std::string GetText();
	void SetText(std::string text);
	hgeFont* GetFont();
	void SetFont(hgeFont* font);

	void SetFont(const char* font);

	void SetSheet(hgeSprite* sprite);

	virtual void	Render();

private:
	hgeFont*		mNormalFont;
	hgeFont*		mBoldFont;
	hgeFont*		mFont;
	float			tx, ty;
	int				mAlign;
	std::string		mText;
	hgeRect			mPosition;
	hgeSprite*		mSheet;
	FormatedText	mFormatedText;
};

#endif