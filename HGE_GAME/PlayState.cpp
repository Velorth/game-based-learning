#include "Engine.h"
#include "resource.h"

PlayState::PlayState()
{
	Finished = new Event();
	isPaused = false;
	// GUI
	HTEXTURE texture;
	HEFFECT clickEffect = Engine::GetSingleton()->GetHGE()->Effect_Load("../Data/Sounds/CLICK.WAV");
	mCodeView = new TextView();
	mTaskView = new FormatedView();
	mScoreView = new TextView();
	hgeFont* font = new hgeFont("../Data/Fonts/font1.fnt");

	font->SetColor(MY_CODE_COLOUR);
	font->SetScale(1.0f);

	mTaskView->SetMode(HGETEXT_LEFT);
	mTaskView->SetFont(new hgeFont("../Data/Fonts/font2.fnt"));
	mTaskView->GetFont()->SetColor(MY_TASK_COLOUR);
	mTaskView->SetPosition(32, 374, 240, 200);
	//mTaskView->SetSheet(sprite);

	mCodeView->SetMode(HGETEXT_LEFT);
	mCodeView->SetFont(font);
	mCodeView->SetPosition(422, 374, 345, 200);
	//mCodeView->SetSheet(sprite);
	mCodeView->SetText("");

	mGUI->AddCtrl(mCodeView);
	mGUI->AddCtrl(mTaskView);
	texture = Engine::GetSingleton()->GetResourceManager()->GetTexture("../DATA/TEXTURES/CURSOR.PNG");
	mCursor = new hgeSprite(texture, 0, 0, 16, 16);

	mGUI->SetCursor(mCursor);

	mField = new SnakeField();
	((SnakeField*)mField)->TokenChosen->Add(new EventHandler(this, (EventHandlerFunction)&PlayState::OnTokenChosen));
	((SnakeField*)mField)->TaskFinished->Add(new EventHandler(this, (EventHandlerFunction)&PlayState::OnTaskFinished));
	//mSnake = new Snake((Field*)mField);

	HTEXTURE buttons = Engine::GetSingleton()->GetResourceManager()->GetTexture("../Data/Textures/Buttons.png");

	mPauseButton = new CheckButton(294, 426, 74, 68, buttons, 96, 68, 170, 68);
	mPauseButton->Check->Add(new EventHandler(this, (EventHandlerFunction)&PlayState::OnPlayPause));
	mPauseButton->SetHighlightedSprite(buttons,96,1,170,1);
	mPauseButton->SetHotKey(HGEK_SPACE);
	mPauseButton->SetClickSound(clickEffect);

	mUseSound = new CheckButton(340, 472, 54, 48, buttons, 244, 48, 298, 48);
	mUseSound->SetHighlightedSprite(buttons, 244,1,298,1);
	mUseSound->Check->Add(NewEventHandler(this, &PlayState::OnUseSoundClicked));
	mUseSound->SetClickSound(clickEffect);

	mMenuButton = new PushButton(292, 506, 96,84, buttons, 0, 84);
	mMenuButton->SetHighlightedSprite(buttons, 0, 0);
	mMenuButton->Click->Add(NewEventHandler(this, &PlayState::OnMenuClicked));
	mMenuButton->SetHotKey(HGEK_ESCAPE);
	mMenuButton->SetClickSound(clickEffect);

	mScoreView->SetPosition(373, 390, 100, 40);
	mScoreView->SetFont("../Data/Fonts/Font2.fnt");
	mScoreView->SetMode(HGETEXT_CENTER);
	mScoreView->GetFont()->SetColor(0xff000000);
	char text[20];
	itoa(XCourse::GetSingleton()->GetScore(), text, 10);
	mScoreView->SetText(text);

	mGUI->AddCtrl(mPauseButton);
	mGUI->AddCtrl(mMenuButton);
	mGUI->AddCtrl(mUseSound);
	mGUI->AddCtrl(mScoreView);

	mVictoryState = new VictoryState();
	mVictoryState->Done->Add(NewEventHandler(this, &PlayState::OnDone));
	aboutState = new AboutState();

	mGameDoneMessage		= GetStringResource(IDS_GAME_DONE);
	mGameOverMessage		= GetStringResource(IDS_GAME_OVER);
	mLevelCompliteMessage	= GetStringResource(IDS_LEVEL_COMPLITE);
}
void PlayState::OnMenuClicked(void*)
{
	XCourse::GetSingleton()->SaveProgress();
	Finished->Invoke(this);
	Close();
}
void PlayState::OnStart()
{
	if (XCourse::GetSingleton()->GetScore() == 0)
		Engine::GetSingleton()->GetStateManager()->Push(aboutState);

	isPaused = false;
	isClosed = false;
	XCourse::GetSingleton()->GetTask()->Start();
	((SnakeField*)mField)->Start();
	mTaskView->SetText(XCourse::GetSingleton()->GetTask()->GetDescription());
	mPauseButton->SetChecked(false);
	mCodeView->SetText(XCourse::GetSingleton()->GetTask()->GetBeforeCode());

}
void PlayState::pause() {}
void PlayState::resume() {}
void PlayState::start() {}

void PlayState::OnUpdate(float elapsedTime) 
{
	mUseSound->SetChecked(!(SoundManager::GetSingleton()->GetUseSound()));
	if (!isPaused)
		mField->Update(elapsedTime);
}

bool PlayState::Draw(float elapsedTime) 
{ 
	mField->Draw(elapsedTime);
	return true; 
}

PlayState::~PlayState()
{
	delete aboutState;
	delete mField;
}

void PlayState::OnPlayPause(void* params)
{
	isPaused = !isPaused;
}

void PlayState::OnTokenChosen(void* params)
{
	// ������� �������
	XToken* token = (XToken*)params;

	std::string text = mCodeView->GetText();
	
	if (token->newLine)
		text += "\n";

	for (int i = 0; i < token->tabs; ++i)
		text += "\t";

	text += token->text;

	for (int i = 0; i < token->spaces; ++i)
		text += " ";

	mCodeView->SetText(text);

	char score[20];
	itoa(XCourse::GetSingleton()->GetScore(), score, 10);
	mScoreView->SetText(score);		
}

void PlayState::OnTaskFinished(void* params)
{
	XToken* lastToken = (XToken*)params;

	bool correct = lastToken ? lastToken->correct : false;
	if (correct)
	{
		char text[20];
		itoa(XCourse::GetSingleton()->GetScore(), text, 10);
		mScoreView->SetText(text);

		XCourse::GetSingleton()->StartNextTask();

		mVictoryState->SetMessage("Level done");
	}
	else
	{
		mVictoryState->SetMessageText("You are loser...\nTry again?");
	}

	XCourse::GetSingleton()->SaveProgress();
	Engine::GetSingleton()->GetStateManager()->Push(mVictoryState);
}

void PlayState::OnDone(void* params)
{
	bool tryAgain = (*(bool*)params);
	if (tryAgain)
	{
		XCourse::GetSingleton()->GetTask()->Start();
		mCodeView->SetText(XCourse::GetSingleton()->GetTask()->GetBeforeCode());
		mTaskView->SetText(XCourse::GetSingleton()->GetTask()->GetDescription());
		((SnakeField*)mField)->Start();
	}
	else
	{
		Finished->Invoke(this);
		Close();
	}
}

void PlayState::OnUseSoundClicked(void*)
{
	SoundManager* soundManager = SoundManager::GetSingleton();
	soundManager->SetUseSound(!soundManager->GetUseSound());
}