#ifndef STATE_H
#define STATE_H

#include <hgegui.h>
#include <hgeguictrls.h>
#include "Object.h"
#include "GUI.h"

class State : public Object
{
public:
	State()
	{
		mGUI = new GUI();
		mPausePrev = true;
		isTransporate = true;
		isClosed = false;
	}

	GUI* GetGUI()
	{
		return mGUI;
	}

	virtual void pause() = 0;
	virtual void resume() = 0;

	virtual void start() = 0;
	void Close()
	{
		isClosed = true;
	}

	bool Update(float elapsedTime)
	{
		OnUpdate(elapsedTime);
		GetGUI()->Update(elapsedTime);
		return !isClosed;
	}
	virtual void OnUpdate(float elapsedTime) = 0;
	virtual void OnStart() {isClosed = false;}
	virtual void OnResume() {}
	virtual bool Draw(float elapsedTime) = 0;

	virtual ~State() 
	{
		delete mGUI;
	}

	bool IsTransporate()
	{
		return isTransporate;
	}

	bool GetPausePrev()
	{
		return mPausePrev;
	}
	void SetPausePrev(bool value)
	{
		mPausePrev = value;
	}
protected:
	GUI* mGUI;
	bool mPausePrev;
	bool isTransporate;
	bool isClosed;
};


#endif