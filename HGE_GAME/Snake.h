#ifndef SNAKE_H
#define SNAKE_H

#include <vector>
#include <set>
#include <math.h>
#include "Object.h"
#include "Engine.h"
#include "Level.h"
#include "ComplexObject.h"
#include "Learning.h"
#include "Entity.h"
#include "TextView.h"

//class SegmentModel : public Model
//{
//public:
//	SegmentModel(hgeSprite* sprite);
//
//	virtual ~SegmentModel();
//
//	void SetSprite(hgeSprite* sprite);
//	float GetSizeX();
//	float GetSizeY();
//	void SetSize(float sizex, float sizey);
//	virtual void Render(float x, float y, float angle);
//
//	virtual void Update(float elapsedTime);
//protected:
//	hgeSprite* mSprite;
//	float mSizeX;
//	float mSizeY;
//
//};

class FoodModel : public SpriteModel
{
public:
	FoodModel();
	virtual void Render(float x, float y, float angle);
	void SetText(std::string text);
protected:
	hgeSprite* mTokenSprite;
	std::string mText;
	hgeFont* mFont;
	int mLength;
};
//class Segment : public Entity
//{
//public:
//	//SegmentModel* mLineModel;
//	//SegmentModel* mRCornerModel;
//	//SegmentModel* mLCornerModel;
//
//	Segment(int direction);
//	virtual ~Segment();
//
//	virtual void Update(float elapsedTime);
//
//	int	GetDirection();
//	void SetDirection(int direction);
//	void SetDirection(int direction, int oldDirection);
//protected:
//	int x;
//	int y;
//	int mDirection;
//	int mPrevDirection;
//};

class Food : public Entity
{
public:
	Food();

	/// ������������� ������� ��� ���
	void		SetToken(XToken* token);

	/// ���������� ������� ���
	XToken*		GetToken();

	/// ��������� ���
	void Update(float elapsedTime);

	Vector2 cell_coords;
protected:
	XToken*	mToken;
};

//
//class SnakeSegment
//{
//public:
//	SnakeSegment();
//	virtual ~SnakeSegment();
//
//	virtual void RenderSegment(float x, float y, float phase, int dir, int rotDir, int kind1, int kindNext=-1);
//	virtual void Render(float x, float y, float phase, int dir, int rotDir, bool isTail);
//
//	//virtual void Update(float elapsedTime);
//protected:
//	hgeSprite *mTail, *mHead, *mBody[16];
//	HTEXTURE mTexture;
//};
//
class SnakeField : public Level
{
public:
	SnakeField();
	virtual ~SnakeField();
	void Start();
	void Resume();
	virtual void Update(float elapsedTime);
	//virtual void Draw(float elapsedTime);
	void DrawSnake();
	Event* TaskFinished;
	Event* TokenChosen;
protected:
	ObjectList* mSnake;
	Food* mFood;
	int mFoodCount;
	Layer* mFieldLayer;
	Layer* mFoodLayer;
	Layer* mSnakeLayer;
	Layer* mGrassLayer;
	Entity* mBaground;
	Entity* mGrass;
	//Segment* mHead;

	void GenerateFood();
	
	void MoveSnake(float elapsedTime);
	
	bool isSelfEat();
	bool isSnakeAtPoint(const Vector2 &p);
	Food* getEatenFood();
	Food* getFoodAtPoint(const Vector2 &p);

	hgeSprite*  mHeadSprite;

	void SetDirection();
	
	void AddSegment();

	float mSnakeSpeed;
	int mHeadDirection, mNewDirection;

	std::vector<Vector2> mSnakePoints;

	float mSnakePhase;
	void DrawSnakeSegment(float x, float y, float phase, int dir, int rotDir, int kind1, int kindNext=-1);
	HTEXTURE mSnakeTexture;
	hgeSprite *mSnakeSprite;
};

class SnakeLayer : public Layer
{
public:
	SnakeLayer(SnakeField*f) : mField(f) {};

	virtual void Draw(float, float)
	{
		mField->DrawSnake();
	};
protected:
	SnakeField *mField;
};

#endif
