#include "Engine.h"

PushButton::PushButton()
	{
		mHotKey = 0;
		hkPressed = false;
		id = (DWORD)this;

		bStatic=false;
		bVisible=true;
		bEnabled=true;

		isOver = false;
		isPressed = false;
		wasPressed = false;
		mSender = 0;

			////
		mSender = 0;
		mSprite = 0;
		mHighlightedSprite = 0;
		mClickedSprite = 0;
		Click = new Event();
	}

void PushButton::SetPosition(float x, float y, float w, float h)
{
	rect.x1 = x;
	rect.y1 = y;
	rect.x2 = x + w;
	rect.y2 = y + h;
}
void PushButton::SetSprite(const char* texture, float x, float y, float w, float h)
{
	if (mSprite != 0)
		delete mSprite;
	mSprite = new hgeSprite(Engine::GetSingleton()->GetResourceManager()->GetTexture(texture), x, y, w, h);
}

void PushButton::SetClickedSprite(const char* texture, float x, float y, float w, float h)
{
	if (mClickedSprite != 0)
		delete mClickedSprite;
	mClickedSprite = new hgeSprite(Engine::GetSingleton()->GetResourceManager()->GetTexture(texture), x, y, w, h);
}

void PushButton::SetHighlightedSprite(const char* texture, float x, float y, float w, float h)
{
	if (mHighlightedSprite != 0)
		delete mHighlightedSprite;
	mHighlightedSprite = new hgeSprite(Engine::GetSingleton()->GetResourceManager()->GetTexture(texture), x, y, w, h);
}

PushButton::PushButton(float x, float y, float w, float h, HTEXTURE tex, float tx, float ty) 
{
	id = (DWORD)this;

	rect.x1 = x;
	rect.y1 = y;
	rect.x2 = x + w;
	rect.y2 = y + h;

	bStatic=false;
	bVisible=true;
	bEnabled=true;

	isOver = false;
	isPressed = false;
	wasPressed = false;
	mSender = 0;

		

	mSprite = new hgeSprite(tex, tx, ty, w, h);
	mClickedSprite = new hgeSprite(tex, tx, ty, w, h);
	mHighlightedSprite = new hgeSprite(tex, tx, ty, w, h);
	mHotKey = 0;
	hkPressed = false;
	////
	mSender = 0;
	Click = new Event();
}
void PushButton::SetHighlightedSprite(HTEXTURE texture, float tx, float ty)
{
	mHighlightedSprite->SetTexture(texture);
	mHighlightedSprite->SetTextureRect(tx, ty, rect.x2- rect.x1, rect.y2 - rect.y1);
}
PushButton::~PushButton()
{
	delete mSprite;
	delete mClickedSprite;
	delete mHighlightedSprite;
}

bool PushButton::IsClicked()
{
	return (wasPressed && !isPressed && isOver);
}
void PushButton::Update(float dt)
{
	if (!mHotKey)
		return;

	if (hkPressed && !( Engine::GetSingleton()->GetHGE()->Input_GetKeyState(mHotKey)))
	{
		hkPressed = false;
		Click->Invoke(this);
	}

	if (Engine::GetSingleton()->GetHGE()->Input_GetKeyState(mHotKey))
		hkPressed = true;
}
void PushButton::SetClickSound(HEFFECT sound)
{
	mClickSound = sound;
}
void PushButton::SetHotKey(Key key)
{
	mHotKey = key;
}
void PushButton::MouseOver(bool bOver)
{
	isOver = bOver;
}
bool PushButton::MouseLButton(bool bDown)
{
	wasPressed = isPressed;
	isPressed = bDown;

	if (IsClicked())
	{

		Click->Invoke(this);
		SoundManager::GetSingleton()->PlayEffect(mClickSound);

		return false;
	}

	return true;
}
void PushButton::Render()
{
	if (isPressed)
		mClickedSprite->Render(rect.x1, rect.y1);
	else
	{
		if (isOver)
			mHighlightedSprite->Render(rect.x1, rect.y1);
		else
			mSprite->Render(rect.x1, rect.y1);
	}
}