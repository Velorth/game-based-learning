#include "Engine.h"
#include "resource.h"

Engine* Engine::mInstance = 0;

std::string GetStringResource(UINT id)
{
	HINSTANCE hAppInstance=GetModuleHandle(NULL);
	char str[256];
	LoadStringA(hAppInstance, id, str, 256);
	
	return std::string(str);
}

HGE* Engine::GetHGE()
{
	return mHGE;
}

void Engine::ShowLogo()
{
	Sleep(200);
	mLogo = new LogoState(new hgeSprite(mResourceManager->GetTexture("../Data/Textures/Logo.png"),0,0,954, 755));
	mStateManager->Push(mLogo);
	mHGE->System_Start();
	Sleep(200);
}

hgeResourceManager* Engine::GetResourceManager()
{
	return mResourceManager;
}

void	Engine::Start(State* startState)
{
	mStateManager->Push(startState);
	mHGE->System_Start();
}

Engine*	Engine::GetSingleton()
{
	if (mInstance)
		return mInstance;

	mInstance = new Engine();
	return mInstance;
}

StateManager* Engine::GetStateManager()
{
	return mStateManager;
}

Engine::Engine()
{
	mHGE = 0;
	mResourceManager = 0;
	SetupHGE();

	mStateManager = new StateManager;

	mInstance = this;
}

Engine::~Engine()
{
	if (mHGE)
	{
		mHGE->Release();
		mHGE = 0;
	}

	if (mResourceManager)
	{
		delete mResourceManager;
		mResourceManager = 0;
	}
}

// ������������� HGE
bool Engine::SetupHGE()
{
	// ������� HGE
	if (!(mHGE = hgeCreate(HGE_VERSION)))
	{
		MessageBoxA(0, "���������� ������� ��������� HGE", "������", 0);
		return false;
	}

	// ������ ����������
	mHGE->System_SetState(HGE_INIFILE, "HGE.ini");

	bool	windowed = mHGE->Ini_GetInt("PARAMS", "Windowed", 1) != 0;
	int mWidth = mHGE->Ini_GetInt("PARAMS", "Width", 800);
	int mHeight = mHGE->Ini_GetInt("PARAMS", "Height", 3 * mWidth / 4);
	int		color = mHGE->Ini_GetInt("PARAMS", "Color", 32);
	char*	windowTitle = mHGE->Ini_GetString("WINDOW", "Title", "");

	// �������� ��������� HGE
	mHGE->System_SetState(HGE_LOGFILE, "HGE.log");			// ���
	mHGE->System_SetState(HGE_FRAMEFUNC, FrameFunction);	// ������� �� ����� ����� (�� ���� ��� ���������)
	mHGE->System_SetState(HGE_RENDERFUNC, RenderFunction);	// ������� ��������� �����
	mHGE->System_SetState(HGE_TITLE, windowTitle);			// ��������� ����
	mHGE->System_SetState(HGE_WINDOWED, windowed);			// ������� �����
	mHGE->System_SetState(HGE_SCREENWIDTH, mWidth);			// ������ ����
	mHGE->System_SetState(HGE_SCREENHEIGHT, mHeight);		// ������ ����
	mHGE->System_SetState(HGE_SCREENBPP, color);			// ������� �����
	mHGE->System_SetState(HGE_ZBUFFER, true);				// ������������ Z-�����
	mHGE->System_SetState(HGE_ICON, MAKEINTRESOURCEA(IDI_ICON1));			// ������


	// �������������� HGE
	if(!mHGE->System_Initiate())
	{
		mHGE->System_Shutdown();
		mHGE->Release();
		mHGE = NULL;
		MessageBoxA(0, "������ �������������", "������", 0);
		return false;
	}

	// �������������� �������� ��������
	mResourceManager = new hgeResourceManager();

	SetupGUI();
	return true;
}

void Engine::SetupGUI()
{
	// GUI
		
	//mGUI->Enter();
}

// ���������� ��������� ��������
bool Engine::FrameFunction()
{
	return mInstance->mStateManager->Update(mInstance->mHGE->Timer_GetDelta());
}

// ��������� �����
bool Engine::RenderFunction()
{
	mInstance->mHGE->Gfx_BeginScene();
	mInstance->mHGE->Gfx_Clear(0);

	mInstance->mStateManager->Draw(mInstance->mHGE->Timer_GetDelta());

	mInstance->mHGE->Gfx_EndScene();
	return true;
}