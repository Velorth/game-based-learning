#ifndef PLAYERS_MENU_STATE_H
#define PLAYERS_MENU_STATE_H

#include "State.h"
#include "Engine.h"
#include "PushButton.h"
#include "PlayState.h"
#include "ListWidget.h"
#include "Slider.h"
#include "Learning.h"
#include "SoundManager.h"
#include "Definitions.h"


class PlayersMenuState : public State
{
public:
	PlayersMenuState();

	void OnUseSoundClicked(void*);
	void GenerateLevelsList();

	void OnSliderMoved(void* params);

	virtual void pause();
	virtual void resume();
	virtual void start();
	virtual void OnUpdate(float elapsedTime);
	virtual bool Draw(float elapsedTime);

	virtual ~PlayersMenuState();

	void StartLevel(void* params);
	void BackToMenu(void* params);
	void SetPrisesSprites(void*);
	virtual void OnStart();
protected:
	Level* mLevel;
	Layer* mMenuLayer;
	hgeSprite* mCursor;
	PushButton* mBackToMenu;
	PushButton* mStartButton;
	PushButton* mPrisesButton;
	CheckButton* mUseSound;
	ListWidget* mLevelsList;
	Slider* mSlider;
	TextView* mScoreView;

	Entity* mBackground;
	Entity* mGrass;
	Entity* mTabPage;
	Entity* mPauseButton;

	Entity* mFirst;
	Entity* mHalf;
	Entity* mAll;
	Entity* mMore100;
	Entity* mMore300;
	Entity* mMore500;

	int mCurrentTopic;

	PlayState* mPlayState;

};


#endif