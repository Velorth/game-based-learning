#ifndef COMPLEX_OBJECT_H
#define COMPLEX_OBJECT_H

#include "Object.h"
#include "Model.h"
#include <list>


class ComplexModel : public Model
{
public:
	ComplexModel(ObjectList* objects) 
	{
		mObjects = objects;
	}

	virtual void Render(float x, float y, float angle)
	{
		for (ObjectList::iterator it = mObjects->begin(); it != mObjects->end(); ++it)
		{
			(*it)->GetModel()->Render((*it)->GetX() + x, (*it)->GetY() + y, (*it)->GetRot() + angle);
		}
	}

	virtual void Update(float elapsedTime)
	{
		for (ObjectList::iterator it = mObjects->begin(); it != mObjects->end(); ++it)
		{
			(*it)->GetModel()->Update(elapsedTime);
		}
	}
protected:
	ObjectList* mObjects;
};

class ComplexObject : public Entity
{
public:
	ComplexObject()
	{
		mObjectList = new ObjectList();
		SetModel(new ComplexModel(mObjectList));
	}

	virtual ~ComplexObject()
	{
		delete mObjectList;
	}

	ObjectList* GetObjectList()
	{
		return mObjectList;
	}

	virtual void Update(float elapsedTime)
	{
		for (ObjectList::iterator it = mObjectList->begin(); it != mObjectList->end(); ++it)
		{
			(*it)->Update(elapsedTime);
		}
	}

protected:
	ObjectList* mObjectList;
};

#endif