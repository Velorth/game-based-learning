#include "Engine.h"

bool StateManager::Update(float elapsedTime)
{
	// ������������ �������� ���������
	while (mStates.size() != 0)
	{

		//mStates.back()->GetGUI()->Update(elapsedTime);
		if ((mStates.back()->Update(elapsedTime)))
		{
			mUpdatedStates.push_back(mStates.back());
		}
		
		bool f = mStates.back()->GetPausePrev();
		mStates.pop_back();

		if (f)
			break;
	}

	// ���������� �������� � ����
	while (mUpdatedStates.size() != 0)
	{
		mStates.push_back(mUpdatedStates.back());
		mUpdatedStates.pop_back();
	}

	return mStates.size() == 0;
}

bool StateManager::Draw(float elapsedTime)
{
	// ������������ �������� ���������
	while (mStates.size() != 0)
	{
		
		mUpdatedStates.push_back(mStates.back());
		
		bool f = mStates.back()->IsTransporate();
		mStates.pop_back();

		if (!f)
			break;
	}

	// ���������� �������� � ����
	while (mUpdatedStates.size() != 0)
	{
		mUpdatedStates.back()->Draw(elapsedTime);
		mUpdatedStates.back()->GetGUI()->Render();
		mStates.push_back(mUpdatedStates.back());
		mUpdatedStates.pop_back();
	}

	return false;
}
