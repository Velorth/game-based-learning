#ifndef MENU_H
#define MENU_H

#include "Engine.h"
#include "State.h"
#include "PushButton.h"

class Menu : public State
{
public:
	Menu()
	{
	/*	HTEXTURE texture = Engine::GetResourceManager()->GetTexture("../Data/Textures/Menu_buttons.png");

		mNewGame

		mLevel = new Level();
		HEFFECT sound = Engine::GetSingleton()->GetResourceManager()->GetEffect("../Data/Sounds/CLICK.WAV");
		
		mCursor = new hgeSprite(texture, 0, 0, 16, 16);
		
		mNewGame = new PushButton(100, 100, 200, 50, texture, 0, 0);
		

		mNewGame->Click->Add(new EventHandler(this, (EventHandlerFunction)&MainMenuState::NewGame));
		mNewGame->SetClickSound(sound);
		mNewGame->SetHotKey(HGEK_ESCAPE);

		mLoadGame->Click->Add(new EventHandler(this, (EventHandlerFunction)&MainMenuState::LoadGame));
		mLoadGame->SetClickSound(sound);

		mExit->Click->Add(new EventHandler(this, (EventHandlerFunction)&MainMenuState::ExitGame));
		mExit->SetClickSound(sound);

		GetGUI()->AddCtrl(mNewGame);
		GetGUI()->AddCtrl(mLoadGame);
		GetGUI()->AddCtrl(mExit);
		GetGUI()->AddCtrl(mUseSound);
		GetGUI()->SetCursor(mCursor);*/

	}

	virtual void pause(){}
	virtual void resume(){}
	virtual void start(){}
	virtual void OnUpdate(float elapsedTime){}
	virtual bool Draw(float elapsedTime){}

	virtual ~Menu()
	{
	}
protected:
	PushButton* mNewGame;
	PushButton* mContinueGame;
	PushButton* mExit;

		// �������
	Level* mLevel;

	hgeSprite* mCursor;
};

#endif