#ifndef LOGO_STATE
#define LOGO_STATE

class LogoState : public State
{
public:
	LogoState(hgeSprite* sprite)
	{
		mOfielLogo = sprite;
		mTime = 2.0f;
	}

	virtual void pause(){}
	virtual void resume(){}
	virtual void start(){}
	virtual void OnUpdate(float elapsedTime){mTime -= elapsedTime; if (mTime < 0) Close();}
	virtual bool Draw(float elapsedTime){mOfielLogo->Render4V(0,0,800,0,800,600,0,600);return false;}

	virtual ~LogoState()
	{
	}
protected:
	float mTime;
	hgeSprite* mOfielLogo;
};
#endif