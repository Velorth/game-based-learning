#include "Engine.h"
#include <windows.h>
#include <stdlib.h>
#include <time.h>

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	srand(time(NULL));
	XCourse::GetSingleton()->Load("../Data/Course.xml");
	Engine* engine = Engine::GetSingleton();
	engine->ShowLogo();
	State* menuState = new MainMenuState();
	SoundManager::GetSingleton()->SetBackGroundMusic("../Data/Sounds/Background.wav");
	engine->Start(menuState);
	return 0;
}