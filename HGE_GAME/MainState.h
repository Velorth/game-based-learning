#ifndef MAIN_STATE
#define MAIN_STATE

#include "State.h"
#include "Engine.h"
#include "Level.h"
#include "PushButton.h"

#include "PlayersMenuState.h"
#include "Learning.h"

class MainState : public State
{
public:
	MainState()
	{
		isUsedSound = true;
		mLevel = new Level();
		mMenuLayer = new Layer();
		mPictureLayer = new Layer();
		HTEXTURE texture = Engine::GetSingleton()->GetResourceManager()->GetTexture("../DATA/textures/menu_buttons.png");
		HEFFECT sound = Engine::GetSingleton()->GetResourceManager()->GetEffect("../Data/Sounds/CLICK.WAV");
		mBackSound = Engine::GetSingleton()->GetResourceManager()->GetEffect("../Data/Sounds/BackSound.mp3");

		mBackground = new Entity();
		mGrass = new Entity();

		// BackGround
		mBackground->Move(400, 300);
		mBackground->Resize(800, 600);
		mBackground->Rotate(-3.14 /2);
		SpriteModel* model = new SpriteModel("../DATA/textures/Background.png", 0, 0, 800, 600);
		model->SetSize(800, 600);
		mBackground->SetModel(model);


		// Grass
		mGrass->Move(400, 300);
		mGrass->Resize(800, 600);
		mGrass->Rotate(-3.14 /2);
		SpriteModel* grassModel = new SpriteModel("../DATA/textures/Grass.png", 0, 0, 800, 600);
		grassModel->SetSize(800, 600);
		mGrass->SetModel(grassModel);

		mMenuLayer->Add(mBackground);
		mMenuLayer->Add(mGrass);
		mLevel->GetLayers()->push_back(mMenuLayer);
		
		//mUseSound->SetClickSound(sound);
		//mUseSound->Click->Add(new EventHandler(this, (EventHandlerFunction)&MainState::UseSound));

		//GetGUI()->AddCtrl(mUseSound);

		texture = Engine::GetSingleton()->GetResourceManager()->GetTexture("../DATA/TEXTURES/CURSOR.PNG");
		mCursor = new hgeSprite(texture, 0, 0, 16, 16);
		GetGUI()->SetCursor(mCursor);

		//Engine::GetSingleton()->GetHGE()->Effect_PlayEx(mBackSound, 50, 0, 1, true);
		Course::GetSingleton()->Load("../Data/Course/Task1.txt");
		mMenuState = new MainMenuState();
	}

	virtual void OnStart()
	{
		Engine::GetSingleton()->GetStateManager()->Push(mMenuState);
	}

	virtual void pause()
	{
	}
	virtual void resume(){}
	virtual void start() {}

	virtual void OnUpdate(float elapsedTime)
	{
		
	}

	virtual bool Draw(float elapsedTime)
	{
		mLevel->Draw(elapsedTime);
		return true;
	}

	virtual ~MainState()
	{
		delete mCursor;
	}

	void UseSound(void* params)
	{
		//isUsedSound = !isUsedSound;
		
		//if (isUsedSound)
		//	Engine::GetSingleton()->GetHGE()->Channel_ResumeAll();
		//else
		//	Engine::GetSingleton()->GetHGE()->Channel_PauseAll();
	}
private:
	// �������
	Level* mLevel;

	hgeSprite* mCursor;

	// ����
	Layer* mMenuLayer;
	Layer* mPictureLayer;

	// ������
	PushButton* mUseSound;

	// ���
	Entity* mBackground;
	Entity* mGrass;

	// ����
	HEFFECT mBackSound;
	HMUSIC	mMusic;

	// ������ ���������, ��� �� ����������
	PlayersMenuState* mPlayersMenuState;
	MainMenuState* mMenuState;
	PlayState* mPlayState;
	bool isUsedSound;
};

#endif