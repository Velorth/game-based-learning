#ifndef MAINMENU_H
#define MAINMENU_H

#include "State.h"
#include "Engine.h"
#include "Level.h"
#include "PushButton.h"

#include "PlayersMenuState.h"
#include "Learning.h"
#include "AboutState.h"

class MainMenuState : public State
{
public:
	MainMenuState();

	virtual void pause();
	virtual void resume();
	virtual void start();

	virtual void OnUpdate(float elapsedTime);

	virtual bool Draw(float elapsedTime);
	void About(void*);
	virtual ~MainMenuState();

	void UseSound(void* params);
	void LoadGame(void* params);
	void NewGame(void* params);
	void ExitGame(void* params);
private:
	// �������
	Level* mLevel;

	hgeSprite* mCursor;

	// ����
	Layer* mMenuLayer;
	Layer* mPictureLayer;

	// ������
	PushButton* mNewGame;
	PushButton* mLoadGame;
	PushButton* mAboutButton;
	PushButton* mExit;
	CheckButton* mUseSound;

	// ���
	Entity* mBackground;
	Entity* mGrass;
	Entity* mTabPage;
	Entity* mMenuButton;
	Entity* mPauseButton;

	// ����
	HEFFECT mBackSound;

	// ������ ���������, ��� �� ����������
	PlayersMenuState* mPlayersState;
	PlayState* mPlayState;
	AboutState* aboutState;
	bool isUsedSound;
};

#endif