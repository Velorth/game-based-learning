#include "Engine.h"
#include "FormatedView.h"
FormatedView::FormatedView()
{
	id = (DWORD)this;
	bStatic=false;
	bVisible=true;
	bEnabled=true;
	mFont = 0;
	mSheet = 0;
	mText = "";
}

int FormatedView::GetMode()
{
	return mAlign;
}
void FormatedView::SetMode(int align)
{
	mAlign = align;
}
hgeRect FormatedView::GetPosition()
{
	return mPosition;
}
void FormatedView::SetPosition(float x, float y, float w, float h)
{
	mPosition.x1 = x;
	mPosition.y1 = y;
	mPosition.x2 = x + w;
	mPosition.y2 = y + h;
}
std::string FormatedView::GetText()
{
	return mText;
}
void FormatedView::SetText(std::string text)
{
	mText = text;
	mFormatedText.clear();

	TextPart part;
	char* cText = (char*)(text.c_str());

	while (cText[0])
	{
		unsigned int length = strlen(cText);

		char* boldText = strstr(cText, "<b>");
		if (boldText)
		{
			boldText[0] = 0;
			boldText += 3;
			part.text = cText;
			part.font = mNormalFont;
			mFormatedText.push_back(part);

			char* normalText = strstr(boldText, "</b>");
			if (normalText)
			{
				normalText[0] = 0;
				normalText += 4;

				part.text = boldText;
				part.font = mBoldFont;
				mFormatedText.push_back(part);
			}
			else
			{
				normalText = cText + length;
			}

			cText = normalText;
		}
		else
		{
			part.text = cText;
			part.font = mNormalFont;
			mFormatedText.push_back(part);
			cText += length;
		}

		FormatedText f;
		for (FormatedText::iterator it = mFormatedText.begin(); it != mFormatedText.end(); ++it)
		{
			std::string txt = it->text + " ";

			char* cText = (char*)(txt.c_str());


			while (cText)
			{
				TextPart part;
				part.font = it->font;

				char* sText = strstr(cText, " ");
				if (sText)
				{
					sText[0] = 0;
					part.text = cText;
					part.text += " ";
					f.push_back(part);
					cText = sText[1] ? sText + 1 : 0;
				}
				else
				{
					part.text = cText;
					f.push_back(part);
					cText = 0;
				}
			}
		}

		mFormatedText = f;
	}
}
hgeFont* FormatedView::GetFont()
{
	return mFont;
}
void FormatedView::SetFont(hgeFont* font)
{
	mFont = font;

	mNormalFont = new hgeFont("../Data/Fonts/TNR20.fnt");
	mBoldFont = new hgeFont("../Data/Fonts/TNR20B.fnt");
	mNormalFont->SetColor(MY_TASK_COLOUR);
	mBoldFont->SetColor(MY_TASK_COLOUR);
}

void FormatedView::SetFont(const char* font)
{
	mFont = new hgeFont(font);
}

void FormatedView::SetSheet(hgeSprite* sprite)
{
	mSheet = sprite;
}

void	FormatedView::Render()
{
	if (mSheet != 0)
	{
		mSheet->Render4V(mPosition.x1, mPosition.y1, mPosition.x2, mPosition.y1, mPosition.x2, mPosition.y2, mPosition.x1, mPosition.y2);
	}

	if (mFont != 0)
	{
		//mFont->printf(mPosition.x1, mPosition.y1, mAlign, mText.data());
		float x = mPosition.x1;
		float y = mPosition.y1;
		for (FormatedText::iterator it = mFormatedText.begin(); it != mFormatedText.end(); ++it)
		{
			if (x + it->font->GetStringWidth(it->text.c_str()) > mPosition.x2)
			{
				x = mPosition.x1;
				y += 20.0f;
			}

			it->font->printf(x, y, HGETEXT_LEFT, it->text.c_str());
			x += it->font->GetStringWidth(it->text.c_str());
		}
	}
}

FormatedView::~FormatedView()
{
}