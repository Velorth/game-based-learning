#include "Engine.h"
SoundManager* SoundManager::mInstance = 0;

SoundManager::SoundManager()
{
	mUseSound = true;
	mMusic = 0;
	mMusicChanel = 0;
}
SoundManager::~SoundManager()
{
	if (mMusic)
		Engine::GetSingleton()->GetHGE()->Music_Free(mMusic);
}
SoundManager* SoundManager::GetSingleton()
{
	if (!mInstance)
		mInstance = new SoundManager();

	return mInstance;
}
void SoundManager::SetUseSound(bool use)
{
	mUseSound = use;
	if (!mMusicChanel)
		return;

	if (mUseSound)
	{
		Engine::GetSingleton()->GetHGE()->Channel_Resume(mMusicChanel);
	}
	else
	{
		Engine::GetSingleton()->GetHGE()->Channel_Pause(mMusicChanel);
	}
}
void SoundManager::SetBackGroundMusic(std::string file)
{
	mMusic = Engine::GetSingleton()->GetHGE()->Effect_Load(file.data());
	mMusicChanel = Engine::GetSingleton()->GetHGE()->Effect_PlayEx(mMusic, 100, 50, 1.0f, true);
	if (!mUseSound)
		Engine::GetSingleton()->GetHGE()->Channel_Pause(mMusicChanel);
}
void SoundManager::PlayEffect(HEFFECT effect)
{
	if (mUseSound)
	{
		Engine::GetSingleton()->GetHGE()->Effect_Play(effect);
	}
}
bool SoundManager::GetUseSound()
{
	return mUseSound;
}