#include "Engine.h"

AboutState::AboutState()
{
	isTransporate = true;
	mPausePrev = true;

	mLevel = new Level();
	mMessageBox = new Entity();
	SpriteModel* boxModel = new SpriteModel("../Data/Textures/GameOverForm.png", 0, 0, 300, 200);
	boxModel->SetSize(600, 350);
	mMessageBox->SetModel(boxModel);
	mMessageBox->Move(400, 300);
	mMessageBox->Rotate(-3.14 / 2);

	mCursor = new hgeSprite(Engine::GetSingleton()->GetResourceManager()->GetTexture("../Data/Textures/cursor.png"), 0, 0, 32, 32);

	// mBackGround
	mBackground = new Layer();
	mBackground->Add(mMessageBox);

	// mText
	mText = new TextView();
	mText->SetPosition(400, 200, 200, 100);
	mText->SetMode(HGETEXT_CENTER);
	mText->SetFont("../Data/Fonts/font2.fnt");
	mText->GetFont()->SetColor(MY_FONT_COLOUR);
	mText->SetText("Welcome to Snake++!\n\nIn this game you should eat bugs with tokens.\n Tokens make a code line that should do a task.\n You should take only correct tokens!\n\n\n Click to continue.");

	mLevel->GetLayers()->push_back(mBackground);

	GetGUI()->SetCursor(mCursor);
	GetGUI()->AddCtrl(mText);
}

void AboutState::pause(){}
void AboutState::resume(){}
void AboutState::start(){}
void AboutState::OnUpdate(float elapsedTime)
{
	if (Engine::GetSingleton()->GetHGE()->Input_KeyDown(HGEK_LBUTTON))
		Close();
}

bool AboutState::Draw(float elapsedTime)
{
	mLevel->Draw(elapsedTime);
	return true;
}

AboutState::~AboutState()
{
}
void AboutState::OnStart()
{
	isClosed = false;
}