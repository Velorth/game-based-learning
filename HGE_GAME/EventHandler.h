#ifndef EVENT_HANDLER_H
#define EVENT_HANDLER_H

#include "Object.h"

typedef void (__thiscall Object::* EventHandlerFunction)(void*);

// ����������� ������� ������� delete-�� ����� ������!
class EventHandler
{
public:
	EventHandler(Object* sender, EventHandlerFunction function)
	{
		mSender = sender;
		mFunction = function;
	}

	void Invoke(void* params)
	{
		(mSender->*mFunction)(params);
	}

	virtual ~EventHandler()
	{
	}
protected:
	Object* mSender;
	EventHandlerFunction mFunction;
};

#define NewEventHandler(sender, function) new EventHandler(sender, (EventHandlerFunction)function)
#endif