#ifndef LEVEL_H
#define LEVEL_H

#include <list>
#include "Layer.h"
#include "Camera.h"

class Level
{
public:
	typedef std::list<Layer*>	LayerList;

	Camera*	GetCamera()
	{
		return &mCamera;
	}
	Level()
	{
		mCamera.x = 0;
		mCamera.y = 0;
	}
	virtual ~Level()
	{
	}

	LayerList* GetLayers()
	{
		return &mLayers;
	}

	virtual void Update(float elapsedTime)
	{
		for (std::list<Layer*>::iterator it = mLayers.begin(); it != mLayers.end(); ++it)
		{
			(*it)->Update(elapsedTime);
		}
	}

	virtual void Draw(float elapsedTime)
	{
		for (std::list<Layer*>::iterator it = mLayers.begin(); it != mLayers.end(); ++it)
		{
			(*it)->Update(elapsedTime);
			(*it)->Draw(mCamera.x,mCamera.y);
		}
	}

protected:
private:
	LayerList	mLayers;
	Camera mCamera;
};

#endif