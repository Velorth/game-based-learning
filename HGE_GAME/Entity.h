#ifndef ENTITY_H
#define ENTITY_H

#include "Object.h"
#include "Model.h"

class Entity
{
public:
	Entity() {mX = 0; mY = 0; mRot = 0;}
	Entity(float x, float y, float rot, float a, float b);

	Model*		GetModel() {return mModel;}
	void		SetModel(Model* model) {mModel = model;}

	void		Move(float x, float y) {mX = x; mY = y; }
	void		Rotate(float ang) {mRot = ang; }

	Vector2*		GetBoundRect();
	bool		IntersectWith(Entity* other);

	float		GetX() { return mX; }
	float		GetY() { return mY; }
	float		GetRot() { return mRot; }
	float		GetSizeX() {return mSizeX;}
	float		GetSizeY() {return mSizeY;}
	void		Resize(float a, float b) {mSizeY = a; mSizeX = b;}

	virtual void Update(float elapsedTime)
	{
	}

	virtual ~Entity() {}
protected:
	float		mX;
	float		mY;
	float		mSizeX;
	float		mSizeY;
	float		mRot;
	Vector2		mBoundRect[5];
	Model*		mModel;
};

typedef std::list<Entity*> ObjectList;
typedef void (__thiscall Entity::* ActionFunction)(void*);

class Action
{
public:
	Action(Entity* sender, ActionFunction func)
	{
		mFunction = func;
		mSender = sender;
	}

	void Execute(void* arg)
	{
		(((Entity*)mSender)->*mFunction)(arg);
	}
	virtual ~Action()
	{
	}
protected:
	ActionFunction mFunction;
	Entity* mSender;
};
#endif