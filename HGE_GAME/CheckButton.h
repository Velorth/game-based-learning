#ifndef CheckButton_H
#define CheckButton_H

#include "State.h"
#include <hgeguictrls.h>
#include "SoundManager.h"
#include "IHotKey.h"
#include "Engine.h"

class CheckButton : public GUIObject , public IHotKeyHandler
{
public:
	CheckButton(float x, float y, float w, float h, HTEXTURE tex, float tx, float ty, float cx, float cy);
	void SetHighlightedSprite(HTEXTURE texture, float tx, float ty, float cx, float cy);
	virtual void Update(float dt);
	virtual bool GetChecked();
	virtual void SetChecked(bool value);
	virtual bool IsClicked();
	virtual bool MouseLButton(bool bDown);
	virtual ~CheckButton();

	virtual void Render();
	void SetClickSound(HEFFECT sound);
	virtual void MouseOver(bool bOver);
	virtual void SetHotKey(Key key);
protected:
	bool isChecked;
	HEFFECT mClickSound;
	Key mHotKey;
	bool hkPressed;
	State* mSender;

	bool isOver;
	bool isPressed, wasPressed;
	hgeSprite* mSprite;
	hgeSprite* mClickedSprite;
	hgeSprite* mHSprite;
	hgeSprite* mHClickedSprite;

public:
	Event* Click;
	Event* Check;
};
#endif