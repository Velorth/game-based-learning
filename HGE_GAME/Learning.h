#ifndef Learning_H
#define Learning_H

#include <vector>
#include <map>
#include <list>
#include <set>
#include <iostream> 
#include <tinyxml.h>

#define PRISE_MORE_100 0
#define PRISE_MORE_300 1
#define PRISE_MORE_500 2
#define PRISE_ONE_TOPIC 3
#define PRISE_HALF 4
#define PRISE_ALL 5

class XToken;
class XTask;
class XTopic;

typedef std::vector<XToken*> ElementList;

class XToken
{
public:
	int id;
	int spaces;
	int tabs;
	bool newLine;
	bool correct;
	std::string text;
	ElementList elements;
	int price;

	void Parse(TiXmlElement* tokenElement);
};

typedef std::vector<XTask*> TaskList;

class XTask
{
public:
	XTask();
	virtual ~XTask();

	std::string GetBeforeCode();
	std::string GetDescription();
	bool ChooseToken(int number);

	/// ��������� �� XML-�������� �������� �������
	void Parse(TiXmlElement* taskElement);

	/// ���������� ������� ������ ������
	ElementList* GetCurrentTokens();

	/// ���������� ���������� ������ �� ������ ���� �������
	int	GetStepSize();

	/// ���������� ����� ������� ��� ������� ����
	void	Start();

	/// ���������� ��������� ����� � ����������� �� ���������� �������
	/// ���������� ������������ ������
	bool	ChooseToken(XToken* token);

	/// ������� ���������
	bool	IsFinished();

	/// ������� ���������� ��� ����������� ���������
	bool	IsCorrect();
protected:
	std::string description;
	std::string beforeCode;
	ElementList elements;
	int mStepSize;
	ElementList* mCurrentElements;
	ElementList mCurrentTokens;
	bool		isFinished;
	bool		isCorrect;
	void	GenerateTokens();
};

typedef std::vector<XTopic*> TopicList;

class XTopic
{
public:
	std::string name;
	TaskList tasks;
	int tasksToNext;
	int GetNextTask()
	{
		if (avaibleTasks.size() == 0)
			return rand() % (tasks.size());

		int index = rand() % (avaibleTasks.size()) + 1;
		std::set<int>::iterator it = avaibleTasks.begin();
		for (int i = 0; i < index; ++i)
			++it;
		return *it;
	}
	void SetAsFinished(int task)
	{
		avaibleTasks.insert(task);
	}
	bool IsFinished()
	{
		return avaibleTasks.size() == 0;
	}
	int GetTaskForNext()
	{
		return tasksToNext;
	}
	std::set<int> avaibleTasks;

	void Parse(TiXmlElement* topicElement);
};


class XProfile
{
	friend class XCourse;
public:
	XProfile();
	virtual ~XProfile();
	void Create(TopicList* topicList);
	void Save();
	int GetScore();
	void SetScore(int score);
	void Load(TopicList* topicList);
	void FinishTopic(int topic);
	TopicList* GetTopics();
	bool	IsDone();
	int mDone;
protected:
	TopicList mTopicList;
	TopicList* mAllTopics;
	int mScore;
};

class XCourse
{
public:
	/// ������ ���������� ������� �������� ����
	void StartTopic(int topic);

	TopicList* GetTopics()
	{
		return mProfile->GetTopics();
	}
	static XCourse* GetSingleton()
	{
		if (mInstance == 0)
			mInstance =  new XCourse();
		return mInstance;
	}
	void Load(std::string fileName);
	XTask* GetTask()
	{
		if (mTaskSet.begin() != mTaskSet.end())
			return *(mTaskSet.begin());
		else
			return 0;
	}
	int	GetScore()
	{
		return mProfile->GetScore();
	}
	void AddScore(int points)
	{
		mProfile->SetScore(mProfile->GetScore() + points);
	}
	bool IsPriseOpened(int priseIndex);
	void CreateProfile()
	{
		mProfile->Create(&topics);
	}

	/// ��������� ���������� �������� ������� � ������� � ����������
	void StartNextTask();

	void SaveProgress()
	{
		mProfile->Save();
	}
	void LoadProgress()
	{
		mProfile->Load(&topics);
	}
	ElementList*	GetTokenBase();
private:
	TaskList		mTaskSet;
	ElementList		mTokenBase;
	XCourse()
	{
		mProfile = new XProfile();
	}
	TopicList topics;
	XProfile* mProfile;
	int mTopicIndex;
	static XCourse* mInstance;
	
	void GenerateTaskSet(int topic);
};

#endif