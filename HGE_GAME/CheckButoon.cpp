#include "Engine.h"

CheckButton::CheckButton(float x, float y, float w, float h, HTEXTURE tex, float tx, float ty, float cx, float cy) 
{
	mHotKey = 0;
	hkPressed = false;
	id = (DWORD)this;

	rect.x1 = x;
	rect.y1 = y;
	rect.x2 = x + w;
	rect.y2 = y + h;

	bStatic=false;
	bVisible=true;
	bEnabled=true;

	isOver = false;
	isPressed = false;
	wasPressed = false;
	mSender = 0;

	mSprite = new hgeSprite(tex, tx, ty, w, h);
	mClickedSprite = new hgeSprite(tex, cx, cy, w, h);
	mHSprite = new hgeSprite(tex, tx, ty, w, h);
	mHClickedSprite = new hgeSprite(tex, cx, cy, w, h);

	////
	mSender = 0;
	isChecked = true;
	Click =  new Event();
	Check = new Event();
}
void CheckButton::SetHighlightedSprite(HTEXTURE texture, float tx, float ty, float cx, float cy)
{
	mHClickedSprite->SetTexture(texture);
	mHClickedSprite->SetTextureRect(cx, cy, rect.x2- rect.x1, rect.y2 - rect.y1);
	mHSprite->SetTexture(texture);
	mHSprite->SetTextureRect(tx, ty, rect.x2- rect.x1, rect.y2 - rect.y1);
}
void CheckButton::Update(float dt)
{
	if (!mHotKey)
		return;

	if (hkPressed && !( Engine::GetSingleton()->GetHGE()->Input_GetKeyState(mHotKey)))
	{
		isChecked = !isChecked;
		hkPressed = false;
		Check->Invoke(this);
	}

	if (Engine::GetSingleton()->GetHGE()->Input_GetKeyState(mHotKey))
		hkPressed = true;
}
bool CheckButton::GetChecked()
{
	return isChecked;
}
void CheckButton::SetChecked(bool value)
{
	isChecked = value;
}
bool CheckButton::IsClicked()
{
	return (wasPressed && !isPressed && isOver);
}
bool CheckButton::MouseLButton(bool bDown)
{
	wasPressed = isPressed;
	isPressed = bDown;

	if (IsClicked())
	{
		isChecked = !isChecked;

		Check->Invoke(this);

		SoundManager::GetSingleton()->PlayEffect(mClickSound);
		return false;
	}
	return true;
}
CheckButton::~CheckButton()
{
	delete mSprite;
	delete mHSprite;
	delete mClickedSprite;
	delete mHClickedSprite;
}

void CheckButton::Render()
{
	if (isChecked)
	{
		if (isOver)
			mHClickedSprite->Render(rect.x1, rect.y1);
		else
			mClickedSprite->Render(rect.x1, rect.y1);
	}
	else
	{
		if (isOver)
			mHSprite->Render(rect.x1, rect.y1);
		else
			mSprite->Render(rect.x1, rect.y1);
	}
}
void CheckButton::SetClickSound(HEFFECT sound)
{
	mClickSound = sound;
}
void CheckButton::MouseOver(bool bOver)
{
	isOver = bOver;
}
void CheckButton::SetHotKey(Key key)
{
	mHotKey = key;
}