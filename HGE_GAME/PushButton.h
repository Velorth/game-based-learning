#ifndef PUSHBUTTON_H
#define PUSHBUTTON_H

#include "State.h"
#include <hgeguictrls.h>
#include "Event.h"
#include "EventHandler.h"
#include "IHotKey.h"
#include "GUI.h"
#include "Engine.h"
#include "SoundManager.h"

class PushButton : public GUIObject , public IHotKeyHandler
{
public:

	PushButton();

	void SetPosition(float x, float y, float w, float h);
	void SetSprite(const char* texture, float x, float y, float w, float h);

	void SetClickedSprite(const char* texture, float x, float y, float w, float h);

	void SetHighlightedSprite(const char* texture, float x, float y, float w, float h);

	PushButton(float x, float y, float w, float h, HTEXTURE tex, float tx, float ty);
	void SetHighlightedSprite(HTEXTURE texture, float tx, float ty);
	virtual ~PushButton();

	virtual bool IsClicked();
	virtual void Update(float dt);
	void SetClickSound(HEFFECT sound);
	virtual void SetHotKey(Key key);
	virtual void MouseOver(bool bOver);
	virtual bool MouseLButton(bool bDown);
	virtual void Render();
protected:
	HEFFECT mClickSound;
	int mHotKey;
	State* mSender;

	bool isOver;
	bool isPressed, wasPressed;
	bool hkPressed;
	hgeSprite* mSprite;
	hgeSprite* mClickedSprite;
	hgeSprite* mHighlightedSprite;

public:
	Event* Click;
};


#endif