#ifndef SLIDER_H
#define SLIDER_H

#include "State.h"
#include <hgeguictrls.h>

#include "Engine.h"

class Slider : public GUIObject
{
public:
	Slider();

	int GetMaxValue();

	float GetRatioValue();

	void SetSliderSprite(const char* texture, float x, float y, float w, float h);

	void SetMaxValue(float value);

	void SetVertical(bool v);

	void SetPosition(float x, float y, float w, float h);
	virtual ~Slider();

	void			SetMode(float _fMin, float _fMax, int _mode);

	float			GetValue() const;

	void SetValue(float _fVal);

	void Render();

	bool MouseLButton(bool bDown);

	bool MouseMove(float x, float y);


protected:
	bool			bPressed;
	bool			bVertical;
	int				mode;
	float			fMin, fMax, fVal;
	float			sl_w, sl_h;
	hgeSprite		*sprSlider;
public:
	Event*			PositionChanged;
};

#endif