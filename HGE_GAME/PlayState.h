#ifndef PLAYSTATE_H
#define PLAYSTATE_H

#include "State.h"
#include "Engine.h"
#include "Snake.h"
#include "TextView.h"
#include "FormatedView.h"
#include "CheckButton.h"
#include "VictoryState.h"
#include "Definitions.h"
#include "AboutState.h"


class PlayState : public State
{
public:
	Event* Finished;
	PlayState();
	void OnMenuClicked(void*);
	virtual void OnStart();
	virtual void pause();
	virtual void resume() ;
	virtual void start() ;

	virtual void OnUpdate(float elapsedTime);

	virtual bool Draw(float elapsedTime);

	virtual ~PlayState();

	void OnPlayPause(void* params);

	void OnTokenChosen(void* params);
	void OnTaskFinished(void* params);

	void OnDone(void* params);
	void OnUseSoundClicked(void*);
private:
	Level*	mField;
	bool isPaused;

	// GUI
	FormatedView* mTaskView;
	TextView* mCodeView;
	TextView* mScoreView;
	hgeSprite* mCursor;
	CheckButton* mPauseButton;
	CheckButton* mUseSound;
	PushButton* mMenuButton;
	AboutState* aboutState;

	VictoryState* mVictoryState;

	// ���������
	std::string mLevelCompliteMessage;
	std::string mGameOverMessage;
	std::string mGameDoneMessage;
};

#endif