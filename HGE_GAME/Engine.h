#ifndef ENGINE_H
#define ENGINE_H

#include <windows.h>
#include <string>

std::string GetStringResource(UINT id);

class Model;
class GUI;
class LogoState;
class Action;
class Camera;
class Entity;
class LEvel;
class Layer;
class Engine;
class Event;
class Object;
class SoundManager;
class State;
class StateManager;

class PushButton;
class TextView;
class CheckButton;
class ListWidget;


#include <hge.h>
#include <hgeresource.h>
#include <hgefont.h>
#include <hgegui.h>
#include <hgeguictrls.h>

#include "Model.h"
#include "GUI.h"
#include "State.h"
#include "LogoState.h"
#include "Action.h"
#include "Camera.h"
#include "Definitions.h"
#include "Entity.h"
#include "Level.h"
#include "Layer.h"
#include "LogoState.h"
#include "Object.h"
#include "SoundManager.h"
#include "StateManager.h"
#include "Vector2.h"

#include "PushButton.h"
#include "CheckButton.h"
#include "TextView.h"
#include "ListWidget.h"

#include "AboutState.h"
#include "Definitions.h"
#include "Learning.h"
#include "MainMenuState.h"
#include "Menu.h"
#include "PlayersMenuState.h"
#include "PlayState.h"
#include "PlayersMenuState.h"
#include "Snake.h"
#include "VictoryState.h"

class Engine
{
public:
	HGE* GetHGE();
	void ShowLogo();
	hgeResourceManager* GetResourceManager();

	void	Start(State* startState);

	static Engine*	GetSingleton();

	StateManager* GetStateManager();
protected:
	Engine();

	virtual ~Engine();

	HGE* mHGE;
	hgeResourceManager* mResourceManager;
	StateManager* mStateManager;
	State* mLogo;
private:
	// ��������� ������
	static Engine* mInstance;
	hgeSprite* mOfielLogo;

	// ������������� HGE
	bool SetupHGE();

	void SetupGUI();

	// ���������� ��������� ��������
	static bool FrameFunction();

	// ��������� �����
	static bool RenderFunction();
};

#endif
