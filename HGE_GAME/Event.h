#ifndef EVENT_H
#define EVENT_H

#include "EventHandler.h"
#include <list>

class Event
{
public :
	Event()
	{

	}
	void Add(EventHandler* handler)
	{
		mHandlers.push_back(handler);
	}
	void Remove(EventHandler* handler)
	{
		mHandlers.remove(handler);
	}
	void Invoke(void* params)
	{
		for (std::list<EventHandler*>::iterator it = mHandlers.begin(); it != mHandlers.end(); ++it)
			(*it)->Invoke(params);
	}
	virtual ~Event()
	{
		// ������� ����������� �������
		for (std::list<EventHandler*>::iterator it = mHandlers.begin(); it != mHandlers.end(); ++it)
			delete  *it;

		mHandlers.clear();
	}
protected:
	std::list<EventHandler*> mHandlers;
};

#endif