#include "Engine.h"


MainMenuState::MainMenuState()
{
	isUsedSound = true;
	mLevel = new Level();
	mMenuLayer = new Layer();
	mPictureLayer = new Layer();
	HTEXTURE texture = Engine::GetSingleton()->GetResourceManager()->GetTexture("../DATA/textures/menu_buttons.png");
	HEFFECT sound = Engine::GetSingleton()->GetResourceManager()->GetEffect("../Data/Sounds/CLICK.WAV");

	mNewGame = new PushButton(300, 61, 200, 50, texture, 0, 0);
	mLoadGame = new PushButton(300, 116, 175, 50, texture, 200, 0);
	mAboutButton = new PushButton(300, 171, 140, 50, texture, 0, 100);
	mExit = new PushButton(300, 236, 115, 50, texture, 140, 99);
	mUseSound = new CheckButton(340, 472, 54, 48, Engine::GetSingleton()->GetResourceManager()->GetTexture("../Data/Textures/Buttons.png"), 244, 48, 298, 48);

	mTabPage = new Entity();
	mBackground = new Entity();
	mMenuButton = new Entity();
	mGrass = new Entity();
	mPauseButton = new Entity();

	// mPauseButton
	mPauseButton->Move(331, 460);
	mPauseButton->Resize(74, 68);
	mPauseButton->Rotate(-3.14 /2);
	SpriteModel* pauseButtonModel = new SpriteModel("../DATA/textures/Buttons.png", 170, 68, 74, 68);
	pauseButtonModel->SetSize(74, 68);
	mPauseButton->SetModel(pauseButtonModel);

	// mUseSound
	mUseSound->SetHighlightedSprite(Engine::GetSingleton()->GetResourceManager()->GetTexture("../Data/Textures/Buttons.png"), 244, 0, 298, 0);
	mUseSound->SetChecked(!(SoundManager::GetSingleton()->GetUseSound()));
	
	// mMenuButton
	mMenuButton->Move(340, 548);
	mMenuButton->Resize(96, 84);
	mMenuButton->Rotate(-3.14 /2);
	SpriteModel* menuButtonModel = new SpriteModel("../DATA/textures/Buttons.png", 0, 84, 96, 84);
	menuButtonModel->SetSize(96, 84);
	mMenuButton->SetModel(menuButtonModel);

	// Background
	mBackground->Move(400, 300);
	mBackground->Resize(800, 600);
	mBackground->Rotate(-3.14 /2);
	SpriteModel* backgroundModel = new SpriteModel("../DATA/textures/Background.png", 0, 0, 800, 600);
	backgroundModel->SetSize(800, 600);
	mBackground->SetModel(backgroundModel);

	// Grass
	mGrass->Move(400, 300);
	mGrass->Resize(800, 600);
	mGrass->Rotate(-3.14 /2);
	SpriteModel* grassModel = new SpriteModel("../DATA/textures/Grass.png", 0, 0, 800, 600);
	grassModel->SetSize(800, 600);
	mGrass->SetModel(grassModel);

	// TabPage
	mTabPage->Move(400, 196);
	mTabPage->Resize(600, 320);
	mTabPage->Rotate(-3.14 /2);
	SpriteModel* model = new SpriteModel("../DATA/textures/Under_menu.png", 0, 0, 512, 360);
	model->SetSize(700, 320);
	mTabPage->SetModel(model);

	// mMenuLayer
	mMenuLayer->Add(mBackground);
	mMenuLayer->Add(mMenuButton);
	mMenuLayer->Add(mPauseButton);
	mMenuLayer->Add(mGrass);
	mMenuLayer->Add(mTabPage);

	// mLevel
	mLevel->GetLayers()->push_back(mMenuLayer);
		
	// Use sound button
	mUseSound->SetClickSound(sound);
	mUseSound->Check->Add(NewEventHandler(this, &MainMenuState::UseSound));

	// New game button
	mNewGame->Click->Add(NewEventHandler(this, &MainMenuState::NewGame));
	mNewGame->SetClickSound(sound);
	mNewGame->SetHighlightedSprite(texture, 0, 50);

	// Load game button
	mLoadGame->Click->Add(NewEventHandler(this, &MainMenuState::LoadGame));
	mLoadGame->SetClickSound(sound);
	mLoadGame->SetHighlightedSprite(texture, 200, 50);

	// AboutButton
	mAboutButton->Click->Add(NewEventHandler(this, &MainMenuState::About));
	mAboutButton->SetClickSound(sound);
	mAboutButton->SetHighlightedSprite(texture, 0, 150);
	mAboutButton->SetHotKey(HGEK_F1);

	// Exit button
	mExit->Click->Add(NewEventHandler(this, &MainMenuState::ExitGame));
	mExit->SetClickSound(sound);
	mExit->SetHighlightedSprite(texture, 140, 149);
	mExit->SetHotKey(HGEK_ESCAPE);

	GetGUI()->AddCtrl(mNewGame);
	GetGUI()->AddCtrl(mLoadGame);
	GetGUI()->AddCtrl(mAboutButton);
	GetGUI()->AddCtrl(mExit);
	GetGUI()->AddCtrl(mUseSound);

	texture = Engine::GetSingleton()->GetResourceManager()->GetTexture("../DATA/TEXTURES/CURSOR.PNG");
	mCursor = new hgeSprite(texture, 0, 0, 16, 16);
	GetGUI()->SetCursor(mCursor);
		
	mPlayersState = new PlayersMenuState();
	aboutState = new AboutState();
}

void MainMenuState::pause()
{
}
void MainMenuState::resume(){}
void MainMenuState::start() {}

void MainMenuState::OnUpdate(float elapsedTime)
{
	mUseSound->SetChecked(!(SoundManager::GetSingleton()->GetUseSound()));
}

bool MainMenuState::Draw(float elapsedTime)
{
	mLevel->Draw(elapsedTime);
	return true;
}

void MainMenuState::About(void*)
{
	Engine::GetSingleton()->GetStateManager()->Push(aboutState);
}
MainMenuState::~MainMenuState()
{
	delete aboutState;
	delete mCursor;
}

void MainMenuState::UseSound(void* params)
{
	SoundManager* soundManager = SoundManager::GetSingleton();
	soundManager->SetUseSound(!soundManager->GetUseSound());
}
void MainMenuState::LoadGame(void* params)
{
	// state->LoadSave("save.dat");
	XCourse::GetSingleton()->LoadProgress();
	Engine::GetSingleton()->GetStateManager()->Push(mPlayersState);
}
void MainMenuState::NewGame(void* params)
{
	XCourse::GetSingleton()->CreateProfile();
	Engine::GetSingleton()->GetStateManager()->Push(mPlayersState);
}
void MainMenuState::ExitGame(void* params)
{
	Close();
}