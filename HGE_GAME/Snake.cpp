#include "Engine.h"
#include <vector>
#include <set>
#include <math.h>
#include "Snake.h"

const int cells_count_x = 16;
const int cells_count_y = 7;

const int cell_pixels = 48;

const int bug_pixels = 24;

const int offset_x = 16;
const int offset_y = 16;

int getDirFromPointPairs(const Vector2 &v1, const Vector2 &v2)
{
	if ( v2.y == v1.y )
	{
		if ( v1.x == -1 && v2.x == cells_count_x )  
			return 2;
		else if ( v1.x == cells_count_x && v2.x == -1 )
			return 0;
		else if ( v2.x > v1.x )
			return 0;
		else
			return 2;
	}
	else
	{
		if ( v1.y == -1 && v2.y == cells_count_y )  
			return 3;
		else if ( v1.y == cells_count_y && v2.y == -1 )
			return 1;
		else if ( v2.y > v1.y )
			return 1;
		else
			return 3;
	}
}

Vector2 getMovedVector2(Vector2 p, int direction)
{
	switch (direction)
	{
	case 0:	p.x++; break;
	case 1:	p.y++; break;
	case 2:	p.x--; break;
	case 3:	p.y--; break;
	}
	return p;
}

bool operator==(const Vector2 &a, const Vector2 &b)
{
	return a.x == b.x && a.y == b.y;
}

// FoodModel
FoodModel::FoodModel()
{
	HTEXTURE texture =  Engine::GetSingleton()->GetResourceManager()->GetTexture("../Data/Textures/Sprites.PNG");
	mTokenSprite = new hgeSprite(texture, 448, 0, 64, 64);
	mText = "";
	mFont = new hgeFont("../Data/Fonts/font1.fnt");
	mFont->SetColor(ARGB(0xff, 0x0, 0x0, 0x0));
	mFont->SetScale(1.0f);
	mLength = 4;
}
void FoodModel::Render(float x, float y, float angle)
{
	mTokenSprite->Render4V(x - mLength * 6, y - 64, x + mLength * 6,  y - 64, x + mLength * 6,  y, x - mLength * 6, y);
	SpriteModel::Render(x,y,angle);
	mFont->printf(x, y - 50, HGETEXT_CENTER,mText.data());
}
void FoodModel::SetText(std::string text)
{
	mText = text;
	mLength = text.length() + 3;
}

// Food

Food::Food()
{
	HTEXTURE texture = Engine::GetSingleton()->GetResourceManager()->GetTexture("../Data/Textures/Sprites.PNG");
	hgeSprite* sprite = new hgeSprite(texture, 320, 0, 64, 64);
	FoodModel* model = new FoodModel();
	model->SetSprite(sprite);
	model->SetSize(bug_pixels, bug_pixels);

	SetModel(model);
}

void Food::SetToken(XToken* token)
{
	mToken = token;
	((FoodModel*)GetModel())->SetText(mToken->text);
}

void Food::Update(float elapsedTime){}

XToken*	Food::GetToken()
{
	return mToken;
}

// SnakeField
SnakeField::SnakeField() : Level()
{
	mFood = 0;
	mNewDirection = 0;
	
	// ������� �������
	TaskFinished = new Event();
	TokenChosen = new Event();

	// ����������� �����
	mFieldLayer = new Layer();
	mFoodLayer = new Layer();
	mSnakeLayer = new SnakeLayer(this);
	mSnake = mSnakeLayer->GetObjects();
	mGrassLayer = new Layer();
	mGrass = new Entity();
	mBaground = new Entity();

	SpriteModel* model = new SpriteModel("../Data/Textures/Background.png", 0, 0, 800, 600);
	model->SetSize(800, 600);

	mBaground->SetModel(model);
	mBaground->Move(400,300);
	mBaground->Rotate(-3.14/2);

	model = new SpriteModel("../Data/Textures/Grass.png", 0, 0, 800, 600);
	model->SetSize(800, 600);
	mGrass->SetModel(model);
	mGrass->Move(400,300);
	mGrass->Rotate(-3.14/2);

	GetLayers()->push_back(mFieldLayer);
	GetLayers()->push_back(mSnakeLayer);
	GetLayers()->push_back(mGrassLayer);
	GetLayers()->push_back(mFoodLayer);
		
	mFieldLayer->Add(mBaground);
	mGrassLayer->Add(mGrass);

	mSnakeTexture = Engine::GetSingleton()->GetResourceManager()->GetTexture("../Data/Textures/Snake.PNG");
	mSnakeSprite = new hgeSprite(mSnakeTexture, 0, 0, 64, 64);

	mHeadSprite = new hgeSprite(mSnakeTexture, 192, 0, 64, 64);

}

SnakeField::~SnakeField()
{
	delete TaskFinished;
	delete TokenChosen;
}

void SnakeField::Start()
{

	mSnakePoints.resize(5);

	mSnakePoints[0].x = 5;
	mSnakePoints[0].y = 2;

	mSnakePoints[1].x = 4;
	mSnakePoints[1].y = 2;

	mSnakePoints[2].x = 3;
	mSnakePoints[2].y = 2;

	mSnakePoints[3].x = 2;
	mSnakePoints[3].y = 2;

	mSnakePoints[4].x = 1;
	mSnakePoints[4].y = 2;

	mSnakePhase = 0;
	mSnakeSpeed =  0;

	mHeadDirection = 0;
	mNewDirection = 0;
	

	GenerateFood();
}

void SnakeField::Resume()
{
}

void SnakeField::Update(float elapsedTime)
{
	float et = elapsedTime;
	Level::Update(elapsedTime);

	SetDirection();

	Food*	food = getEatenFood();
	XTask*	task = XCourse::GetSingleton()->GetTask();

	if (food)
	{
		// ������� �������
		task->ChooseToken(food->GetToken());
		TokenChosen->Invoke(food->GetToken());
			
		// ��������� ����� �������
		AddSegment();

		// ���� ������� �����������, ��������� � ����������,
		// � ��������� ������ ���������� ����� �������� �� ����
		if (task->IsFinished())
		{
			TaskFinished->Invoke(food->GetToken());
		}
		else
		{
			GenerateFood();
		}
	}
	else if (isSelfEat())
	{
		// ����� ���� ����. ������ ������?
		TaskFinished->Invoke(0);
	}
	else
	{
		MoveSnake(et);
	}

}

void SnakeField::GenerateFood()
{
	if (mFood != 0)
	{
		for (ObjectList::iterator it = mFoodLayer->GetObjects()->begin(); it != mFoodLayer->GetObjects()->end();)
		{
			it = mFoodLayer->GetObjects()->erase(it);
		}

		delete []mFood;
	}

	ElementList* currentTokens = XCourse::GetSingleton()->GetTask()->GetCurrentTokens();
	mFoodCount = currentTokens->size();
	mFood = new Food[mFoodCount];

	for (int i = 0; i < mFoodCount; ++i)
	{
		mFood[i].cell_coords.x = -1;
	}

	for (int i = 0; i < mFoodCount; ++i)
	{
		mFood[i].SetToken(currentTokens->at(i));
		int seed = rand() % (cells_count_x * cells_count_y);
		bool placed = false;
		
		Vector2 p = mFood[i].cell_coords;
		do
		{
			p.x = seed % cells_count_x;
			p.y = seed / cells_count_x;
			//p.x = rand() % cells_count_x;
			//p.y = rand() % (cells_count_y-1) + 1;

			placed = true;

			if ( p.y < 1 )
				placed = false;

			if ( placed )
				if ( isSnakeAtPoint(p) )
					placed = false;
			
			if ( placed )
				if ( getFoodAtPoint(p) )
					placed = false;
			if ( placed )
				if ( getFoodAtPoint(getMovedVector2(p,0)) )
					placed = false;
			if ( placed )
				if ( getFoodAtPoint(getMovedVector2(p,1)) )
					placed = false;
			if ( placed )
				if ( getFoodAtPoint(getMovedVector2(p,2)) )
					placed = false;
			if ( placed )
				if ( getFoodAtPoint(getMovedVector2(p,3)) )
					placed = false;

			if ( placed )
			{
				Vector2 pn = getMovedVector2(mSnakePoints.front(),mHeadDirection);
				if( abs(pn.x - p.x) < 2 && abs(pn.y - p.y) < 2 )
					placed = false;
			}

			if (!placed)
			{
				++seed;
				if ( seed > cells_count_x * cells_count_y )
					seed = 0;
			}
		}
		while (!placed);

		mFood[i].cell_coords = p;

		mFood[i].Move(cell_pixels * p.x + cell_pixels/2 + offset_x, 
				      cell_pixels * p.y + cell_pixels/2 + offset_y );

		mFoodLayer->GetObjects()->push_back(&mFood[i]);
	}
}

bool SnakeField::isSelfEat()
{
	if ( mSnakePhase < 0.2 )
		return false;
	for ( int i = 1; i < mSnakePoints.size() - 2; ++ i )
		if ( mSnakePoints.front() ==  mSnakePoints[i] )
			return true;
	return false;
}

bool SnakeField::isSnakeAtPoint(const Vector2 &p)
{
	for ( int i = 0; i < mSnakePoints.size() - 2; ++ i )
		if ( p ==  mSnakePoints[i] )
			return true;
	return false;
}

Food* SnakeField::getFoodAtPoint(const Vector2 &p)
{
	for (int i = 0; i < mFoodCount; ++i)
	{
		if (p == mFood[i].cell_coords)
			return &mFood[i];
	}

	return 0;
}

Food* SnakeField::getEatenFood()
{
//	if ( mSnakePhase < 0.2 )
//		return NULL;
	return getFoodAtPoint(mSnakePoints.front());
}

void SnakeField::MoveSnake(float elapsedTime)
{
	int cnt = mSnakePoints.size();

	mSnakeSpeed += elapsedTime * 0.5;
	if (mSnakeSpeed > 1.25)
		mSnakeSpeed = 1.25;

	// Move 
	mSnakePhase += elapsedTime * mSnakeSpeed;

	if ( mSnakePhase > 1 )
	{
		Vector2 p = getMovedVector2(mSnakePoints[0],mHeadDirection);

		if ( p.x == -2 )
			p.x = cells_count_x;
		else
			if ( p.x == cells_count_x+1 )
				p.x = -1;

		if ( p.y == -2 )
			p.y = cells_count_y;
		else
			if ( p.y == cells_count_y+1 )
				p.y = -1;

		int i = 0;
		while ( i < mSnakePoints.size() && !(p == mSnakePoints[i]) )
		{
			std::swap(mSnakePoints[i],p);
			i++;
		}

		mSnakePhase = mSnakePhase - floorf(mSnakePhase);
	}

	if ( mSnakePhase < 0.4 )
	{
		int headDirection = getDirFromPointPairs(mSnakePoints[1],mSnakePoints[0]);

		if ( mSnakePoints.front().x >= 0 && mSnakePoints.front().x < cells_count_x 
			&& mSnakePoints.front().y >= 0 && mSnakePoints.front().y < cells_count_y ) 
		{
			if (mNewDirection == 0 && headDirection != 2)
				mHeadDirection = 0;
			if (mNewDirection == 1 && headDirection != 3)
				mHeadDirection = 1;
			if (mNewDirection == 2 && headDirection != 0)
				mHeadDirection = 2;
			if (mNewDirection == 3 && headDirection != 1)
				mHeadDirection = 3;
		}
	}
}

void SnakeField::SetDirection()
{
	HGE* hge = Engine::GetSingleton()->GetHGE();

	if (hge->Input_GetKeyState(HGEK_RIGHT) && mHeadDirection!= 2)
		mNewDirection = 0;
	if (hge->Input_GetKeyState(HGEK_DOWN) && mHeadDirection != 3)
		mNewDirection = 1;
	if (hge->Input_GetKeyState(HGEK_LEFT) && mHeadDirection != 0)
		mNewDirection = 2;
	if (hge->Input_GetKeyState(HGEK_UP) && mHeadDirection != 1)
		mNewDirection = 3;
}

void SnakeField::AddSegment()
{
	mSnakePoints.resize(mSnakePoints.size()+1);
	for ( int i = mSnakePoints.size()-1; i > 2 ; --i )
		mSnakePoints[i] = mSnakePoints[i-1];
}

//void SnakeField::Draw(float elapsedTime)
//{
//	Level::Draw(elapsedTime);
//	DrawSnake();
//}

void SnakeField::DrawSnake()
{
	float phase = mSnakePhase < 0 ? 0 : mSnakePhase > 1 ? 1 : mSnakePhase;

	int headDir = mHeadDirection;
	int rot;

	const int cnt = mSnakePoints.size();
	for ( int i = 0; i < cnt-1; i++ )
	{
		Vector2 v0 = mSnakePoints[i+1];
		Vector2 v1 = mSnakePoints[i];

		//if ( i > 0 && v1 == mSnakePoints[i-1])
		if ( v0 == v1 )
		{
			// ������ ��� ����������� �������
			phase = 0; // ���� ���� ����� ������������ - 0
			continue;
		}
		
		int tailDir = getDirFromPointPairs(v0,v1);

		rot = headDir - tailDir;
		if ( rot == -3 )
			rot = 1;
		
		int k1 = (i==0) ? 2 : (i==cnt-3) ? 0 : (i==cnt-2) ? -1 : 1;
		int k2 = (i==0) ? -1 : (i==1) ? 2 : (i==cnt-2) ? 0 : 1;

		DrawSnakeSegment(v1.x, v1.y, phase, tailDir, rot, k1, k2);

		headDir = tailDir;
	}

	//float hx = mSnakePoints[1].x * cell_pixels + offset_x;
	//float hy = mSnakePoints[1].y * cell_pixels + offset_y;
	//float hs = (float)cell_pixels / 64.0f;
	//float hr = 0;
	//if ( rot == 0 )
	//{
	//	switch ( mHeadDirection )
	//	{
	//	case 0:
	//		hr = 0;
	//		hx += mSnakePhase * cell_pixels;
	//		break;
	//	case 1:
	//		hr = M_PI_2;
	//		hy += mSnakePhase * cell_pixels;
	//		break;
	//	case 2:
	//		hr = M_PI;
	//		hx -= mSnakePhase * cell_pixels;
	//		break;
	//	case 3:
	//		hr = M_PI_2 * 3;
	//		hy -= mSnakePhase * cell_pixels;
	//		break;
	//	}
	//}

	//mHeadSprite->RenderEx(hx,hy,hr,hs);
	
	//HTEXTURE texture = Engine::GetSingleton()->GetResourceManager()->GetTexture("../Data/Textures/Dot.PNG");
	//hgeSprite* sprite = new hgeSprite(texture, 0, 0, 64, 64);
	//for ( int i = 0; i < cnt; ++ i )
	//{
	//	Vector2 v = mSnakePoints[i];
	//	sprite->Render(v.x*64+16,v.y*64+16);
	//}
}

void SnakeField::DrawSnakeSegment(float x, float y, float phase, int dir, int rotDir, int kind, int kindNext)
{
	const float tcs = 64;

	const float x0 = cell_pixels * x + offset_x;
	const float y0 = cell_pixels * y + offset_y;
	const float x1 = x0 + cell_pixels;
	const float y1 = y0 + cell_pixels;

	if ( rotDir == 0 )
	{
		// Direct

		float cp = cell_pixels * phase;

		if ( kind != -1 )
		{
			mSnakeSprite->SetTextureRect(tcs*kind+tcs-tcs*phase,0,tcs*phase,tcs,false);

			switch (dir)
			{
			case 0:
				mSnakeSprite->Render4V(x0, y0,  x0 + cp, y0,  x0 + cp, y1,  x0, y1);
				break;
			case 1:
				mSnakeSprite->Render4V(x1, y0,  x1, y0 + cp,  x0, y0 + cp,  x0, y0);
				break;
			case 2:
				mSnakeSprite->Render4V(x1, y1,  x1 - cp, y1,  x1 - cp, y0,  x1, y0);
				break;
			case 3:
				mSnakeSprite->Render4V(x0, y1,  x0, y1 - cp,  x1, y1 - cp,  x1, y1);
				break;
			}
		}

		if ( kindNext != -1 )
		{
			mSnakeSprite->SetTextureRect(tcs*kindNext,0,tcs-tcs*phase,tcs,false);

			switch (dir)
			{
			case 0:
				mSnakeSprite->Render4V(x0 + cp, y0,  x1, y0,  x1, y1,  x0 + cp, y1);
				break;
			case 1:
				mSnakeSprite->Render4V(x1, y0 + cp,  x1, y1,  x0, y1,  x0, y0 + cp);
				break;
			case 2:
				mSnakeSprite->Render4V(x1 - cp, y1,  x0, y1,  x0, y0,  x1 - cp, y0);
				break;
			case 3:
				mSnakeSprite->Render4V(x0, y1 - cp,  x0, y0,  x1, y0,  x1, y1 - cp);
				break;
			}
		}
	}
	else
	{
		// Rotated

		const int nseg = 12;
		//const float margin = 12.0f;
		const float margin = cell_pixels/4;
		const float tmargin = tcs/4;

		bool next = (kind == -1);

		float a1 = next ? M_PI_2 * phase : 0;
		float p1 = next ? 0 : tcs - tcs * phase;
		if ( next )
			kind = kindNext;

		while ( a1 < M_PI_2 )
		{
			float p2 = p1 + tcs / nseg;
			if ( p2 > tcs )
				p2 = tcs;

			float a2 = a1 + M_PI_2 / nseg;

			bool switchToNext = false;
			if ( next )
			{
				if ( a2 > M_PI_2 )
					a2 = M_PI_2 + 0.0001;
			}
			else
			{
				if ( a2 > M_PI_2 * phase )
				{
					a2 = M_PI_2 * phase;
					switchToNext = true;
				}
			}
			
			const float r1 = margin;
			const float r2 = cell_pixels-margin;

			float p0x = r2 * sin(a1);
			float p0y = r2 * cos(a1);

			float p1x = r2 * sin(a2);
			float p1y = r2 * cos(a2);

			float p2x = r1 * sin(a2);
			float p2y = r1 * cos(a2);

			float p3x = r1 * sin(a1);
			float p3y = r1 * cos(a1);

			mSnakeSprite->SetTextureRect(tcs*kind + p1, tmargin, p2 - p1, tcs-tmargin-tmargin, false);

			if ( rotDir == 1 )
			{
				switch ( dir )
				{
				case 0:
					mSnakeSprite->Render4V(x0 + p0x, y1 - p0y, x0 + p1x, y1 - p1y, x0 + p2x, y1 - p2y, x0 + p3x, y1 - p3y);
					break;
				case 1:
					mSnakeSprite->Render4V(x0 + p0y, y0 + p0x, x0 + p1y, y0 + p1x, x0 + p2y, y0 + p2x, x0 + p3y, y0 + p3x);
					break;
				case 2:
					mSnakeSprite->Render4V(x1 - p0x, y0 + p0y, x1 - p1x, y0 + p1y, x1 - p2x, y0 + p2y, x1 - p3x, y0 + p3y);
					break;
				case 3:
					mSnakeSprite->Render4V(x1 - p0y, y1 - p0x, x1 - p1y, y1 - p1x, x1 - p2y, y1 - p2x, x1 - p3y, y1 - p3x);
					break;
				}
			}
			else
			{
				switch ( dir )
				{
				case 0:
					mSnakeSprite->Render4V(x0 + p3x, y0 + p3y, x0 + p2x, y0 + p2y, x0 + p1x, y0 + p1y, x0 + p0x, y0 + p0y);
					break;
				case 1:
					mSnakeSprite->Render4V(x1 - p3y, y0 + p3x, x1 - p2y, y0 + p2x, x1 - p1y, y0 + p1x, x1 - p0y, y0 + p0x);
					break;
				case 2:
					mSnakeSprite->Render4V(x1 - p3x, y1 - p3y, x1 - p2x, y1 - p2y, x1 - p1x, y1 - p1y, x1 - p0x, y1 - p0y);
					break;
				case 3:
					mSnakeSprite->Render4V(x0 + p3y, y1 - p3x, x0 + p2y, y1 - p2x, x0 + p1y, y1 - p1x, x0 + p0y, y1 - p0x);
					break;
				}
			}

			if ( switchToNext )
			{
				if ( kindNext == -1 ) 
					break;
				kind = kindNext;
				next = true;
				p1 = 0;
			}
			else
			{
				p1 = p2;
			}

			a1 = a2;
		}
	}
}
