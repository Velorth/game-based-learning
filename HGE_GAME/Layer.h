#ifndef LAYER_H
#define LAYER_H

#include <list>
#include "Object.h"
#include "Entity.h"

class Layer
{
public:

	Layer()
	{
	};


	void Add(Entity* object)
	{
		mObjects.push_back(object);
	}

	void Remove(Entity* object)
	{
		mObjects.remove(object);
	}

	virtual void Update(float elapsedTime)
	{
		for (std::list <Entity*>::iterator it = mObjects.begin(); it != mObjects.end(); ++it)
		{
			(*it)->Update(elapsedTime);
		}
	}
	virtual void Draw(float offsetx, float offsety)
	{
		for (ObjectList::iterator i = GetObjects()->begin(); i != GetObjects()->end(); ++i)
		{
			Model* model = (*i)->GetModel();
			model->Render((*i)->GetX() + offsetx, (*i)->GetY() + offsety, (*i)->GetRot());
		}
	}


	ObjectList* GetObjects()
	{
		return &mObjects;
	}
	virtual ~Layer()
	{
	}

protected:
private:
	ObjectList mObjects;
};

#endif