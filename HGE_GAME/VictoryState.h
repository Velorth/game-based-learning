#ifndef VICTORYSTATE_H
#define VICTORYSTATE_H

#include "State.h"
#include "Entity.h"
#include "Engine.h"
#include "Model.h"
#include "Definitions.h"

class VictoryState : public State
{
public:
	Event* Done;
	void SetMessageText(std::string text);
	VictoryState();

	void OnCancelClicked(void* param);

	void OnOkClicked(void* param);
	void SetMessage(std::string text);
	virtual void pause();
	virtual void resume();
	virtual void start();
	virtual void OnUpdate(float elapsedTime);
	virtual bool Draw(float elapsedTime);

	virtual ~VictoryState();
protected:
	Entity* mMessageBox;
	PushButton* mOk;
	PushButton* mCancel;
	TextView* mText;

		// �������
	Level* mLevel;
	Layer* mBackground;

	hgeSprite* mCursor;
};

#endif