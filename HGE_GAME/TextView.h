#ifndef TEXTVIEW_H
#define TEXTVIEW_H

#include <string>

class TextView : public GUIObject
{
public:
	TextView()
	{
		id = (DWORD)this;
		bStatic=false;
		bVisible=true;
		bEnabled=true;
		mFont = 0;
		mSheet = 0;
		mText = "";
	}

	int GetMode()
	{
		return mAlign;
	}
	void SetMode(int align)
	{
		mAlign = align;
	}
	hgeRect GetPosition()
	{
		return mPosition;
	}
	void SetPosition(float x, float y, float w, float h)
	{
		mPosition.x1 = x;
		mPosition.y1 = y;
		mPosition.x2 = x + w;
		mPosition.y2 = y + h;
	}
	std::string GetText()
	{
		return mText;
	}
	void SetText(std::string text)
	{
		mText = text;
	}
	hgeFont* GetFont()
	{
		return mFont;
	}
	void SetFont(hgeFont* font)
	{
		mFont = font;
	}

	void SetFont(const char* font)
	{
		mFont = new hgeFont(font);
	}

	void SetSheet(hgeSprite* sprite)
	{
		mSheet = sprite;
	}

	virtual void	Render()
	{
		if (mSheet != 0)
		{
			mSheet->Render4V(mPosition.x1, mPosition.y1, mPosition.x2, mPosition.y1, mPosition.x2, mPosition.y2, mPosition.x1, mPosition.y2);
		}

		if (mFont != 0)
		{
			mFont->printf(mPosition.x1, mPosition.y1, mAlign, mText.data());
		}
	}

	virtual ~TextView()
	{
	}
private:
	hgeFont*		mFont;
	float			tx, ty;
	int				mAlign;
	std::string		mText;
	hgeRect			mPosition;
	hgeSprite*		mSheet;
};
#endif