#ifndef ABOUTSTATE_H
#define ABOUTSTATE_H

#include "Engine.h"

class AboutState : public State
{
public:
	AboutState();

	virtual void pause();
	virtual void resume();
	virtual void start();
	virtual void OnUpdate(float elapsedTime);

	virtual bool Draw(float elapsedTime);

	virtual ~AboutState();
	virtual void OnStart();
protected:
	Level*	mLevel;
	Layer* mBackground;

	Entity* mMessageBox;
	Entity* mPlay;
	Entity* mBug;
	Entity* mBoard;

	// GUI
	TextView* mText;
	hgeSprite* mCursor;
};

#endif