#include "Engine.h"

PlayersMenuState::PlayersMenuState()
{
	mCurrentTopic = 0;
	mLevel = new Level();
	mMenuLayer = new Layer();
	HTEXTURE texture = Engine::GetSingleton()->GetResourceManager()->GetTexture("../DATA/textures/Arrows.png");
	HEFFECT sound = Engine::GetSingleton()->GetResourceManager()->GetEffect("../Data/Sounds/CLICK.WAV");
	texture = Engine::GetSingleton()->GetResourceManager()->GetTexture("../DATA/textures/Buttons.png");
	mStartButton = new PushButton(294, 426, 74, 68,texture, 170, 68);
	mBackToMenu = new PushButton(292, 506, 96, 84, texture, 0, 84);
	mLevelsList = new ListWidget(64, 64, 424, 230, 0);
	mUseSound = new CheckButton(340, 472, 54, 48, texture, 244, 48, 298, 48);


	mSlider = new Slider();
	texture = Engine::GetSingleton()->GetResourceManager()->GetTexture("../DATA/TEXTURES/CURSOR.PNG");
	mCursor = new hgeSprite(texture, 0, 0, 16, 16);
	mTabPage = new Entity();
	mBackground = new Entity();
	mGrass = new Entity();
	mPauseButton = new Entity();

	// �������
	mFirst = new Entity();
	SpriteModel* firstModel = new SpriteModel("../Data/Textures/Awards.png", 0, 0, 72, 72);
	firstModel->SetSize(72, 72);
	mFirst->SetModel(firstModel);
	mFirst->Rotate(-3.14 /2);
	mFirst->Move(540, 104);

	mHalf = new Entity();
	SpriteModel* halfModel = new SpriteModel("../Data/Textures/Awards.png", 72, 0, 72, 72);
	halfModel->SetSize(72, 72);
	mHalf->SetModel(halfModel);
	mHalf->Rotate(-3.14 /2);
	mHalf->Move(612, 104);

	mAll = new Entity();
	SpriteModel* allModel = new SpriteModel("../Data/Textures/Awards.png", 144, 0, 72, 72);
	allModel->SetSize(72, 72);
	mAll->SetModel(allModel);
	mAll->Rotate(-3.14 /2);
	mAll->Move(684, 104);

	mMore100 = new Entity();
	SpriteModel* more100Model = new SpriteModel("../Data/Textures/Awards.png", 216, 0, 72, 72);
	more100Model->SetSize(72, 72);
	mMore100->SetModel(more100Model);
	mMore100->Rotate(-3.14 /2);
	mMore100->Move(540, 176);

	mMore300 = new Entity();
	SpriteModel* more300Model = new SpriteModel("../Data/Textures/Awards.png", 288, 0, 72, 72);
	more300Model->SetSize(72, 72);
	mMore300->SetModel(more300Model);
	mMore300->Rotate(-3.14 /2);
	mMore300->Move(612, 176);

	mMore500 = new Entity();
	SpriteModel* more500Model = new SpriteModel("../Data/Textures/Awards.png", 360, 0, 72, 72);
	more500Model->SetSize(72, 72);
	mMore500->SetModel(more500Model);
	mMore500->Rotate(-3.14 /2);
	mMore500->Move(684, 176);

	// mSlider
	mSlider->SetSliderSprite("../Data/Textures/Slider.png", 0, 0, 28, 65);
	mSlider->SetPosition(454, 66, 40, 230);
	mSlider->SetMode(0, 265, HGESLIDER_SLIDER);
	mSlider->PositionChanged->Add(new EventHandler(this, (EventHandlerFunction)&PlayersMenuState::OnSliderMoved));

	// mStartButton
	mStartButton->SetHighlightedSprite(Engine::GetSingleton()->GetResourceManager()->GetTexture("../Data/Textures/Buttons.png"), 170, 1);
	mStartButton->SetClickSound(sound);
	mStartButton->Click->Add(new EventHandler(this, (EventHandlerFunction)&PlayersMenuState::StartLevel));
	mStartButton->SetHotKey(HGEK_ENTER);

	// mUseSound
	mUseSound->SetHighlightedSprite(Engine::GetSingleton()->GetResourceManager()->GetTexture("../Data/Textures/Buttons.png"), 244, 0, 298, 1);
	mUseSound->SetChecked(true);
	mUseSound->Check->Add(NewEventHandler(this, &PlayersMenuState::OnUseSoundClicked));
	mUseSound->SetClickSound(sound);
	
	// mBackToMenu
	mBackToMenu->SetHighlightedSprite(Engine::GetSingleton()->GetResourceManager()->GetTexture("../Data/Textures/Buttons.png"), 0, 0);
	mBackToMenu->Click->Add(new EventHandler(this, (EventHandlerFunction)&PlayersMenuState::BackToMenu));
	mBackToMenu->SetHotKey(HGEK_ESCAPE);
	mBackToMenu->SetClickSound(sound);

	// Background
	mBackground->Move(400, 300);
	mBackground->Resize(800, 600);
	mBackground->Rotate(-3.14 /2);
	SpriteModel* backgroundModel = new SpriteModel("../DATA/textures/Background.png", 0, 0, 800, 600);
	backgroundModel->SetSize(800, 600);
	mBackground->SetModel(backgroundModel);

	// Grass
	mGrass->Move(400, 300);
	mGrass->Resize(800, 600);
	mGrass->Rotate(-3.14 /2);
	SpriteModel* grassModel = new SpriteModel("../DATA/textures/Grass.png", 0, 0, 800, 600);
	grassModel->SetSize(800, 600);
	mGrass->SetModel(grassModel);

	// TabPage
	mTabPage->Move(400, 196);
	mTabPage->Resize(448, 384);
	mTabPage->Rotate(-3.14 /2);
	SpriteModel* model = new SpriteModel("../DATA/textures/Under_menu.png", 0, 0, 512, 360);
	model->SetSize(700, 320);
	mTabPage->SetModel(model);

	// mMenuLayer
	mMenuLayer->Add(mBackground);
	mMenuLayer->Add(mGrass);
	mMenuLayer->Add(mTabPage);
	mMenuLayer->Add(mFirst);
	mMenuLayer->Add(mHalf);
	mMenuLayer->Add(mAll);
	mMenuLayer->Add(mMore100);
	mMenuLayer->Add(mMore300);
	mMenuLayer->Add(mMore500);

	// mLevelsList
	mLevelsList->SetSheetSprite(new hgeSprite(Engine::GetSingleton()->GetResourceManager()->GetTexture("../Data/Textures/List.png"), 0, 0, 427, 265));
	mLevelsList->SetHighlightSprite(new hgeSprite(Engine::GetSingleton()->GetResourceManager()->GetTexture("../Data/Textures/Select.png"), 0, 0, 384, 45));
	mLevelsList->SetRowSize(45);
	hgeFont* mFont = new hgeFont("../Data/Fonts/Font2.fnt");
	mFont->SetColor(MY_FONT_COLOUR);
	GenerateLevelsList();

		
	// mLevel
	mLevel->GetLayers()->push_back(mMenuLayer);
		
	// Use sound button
	mUseSound->SetClickSound(sound);
	//mUseSound->Click->Add(new EventHandler(this, (EventHandlerFunction)&MainMenuState::UseSound));



	texture = Engine::GetSingleton()->GetResourceManager()->GetTexture("../DATA/TEXTURES/CURSOR.PNG");
	mCursor = new hgeSprite(texture, 0, 0, 16, 16);
	GetGUI()->SetCursor(mCursor);



	mScoreView = new TextView();
	mScoreView->SetPosition(373, 390, 100, 40);
	mScoreView->SetFont("../Data/Fonts/Font2.fnt");
	mScoreView->SetMode(HGETEXT_CENTER);
	mScoreView->GetFont()->SetColor(0xff000000);
	char text[20];
	itoa(XCourse::GetSingleton()->GetScore(), text, 10);
	mScoreView->SetText(text);

	GetGUI()->AddCtrl(mUseSound);
	GetGUI()->AddCtrl(mStartButton);
	GetGUI()->AddCtrl(mBackToMenu);
	GetGUI()->SetCursor(mCursor);
	GetGUI()->AddCtrl(mLevelsList);
	GetGUI()->AddCtrl(mSlider);
	GetGUI()->AddCtrl(mScoreView);

	mPlayState = new PlayState();
	mPlayState->Finished->Add(NewEventHandler(this, &PlayersMenuState::SetPrisesSprites));
}

void PlayersMenuState::OnUseSoundClicked(void*)
{
	SoundManager* soundManager = SoundManager::GetSingleton();
	soundManager->SetUseSound(!soundManager->GetUseSound());
}

void PlayersMenuState::GenerateLevelsList()
{
	mLevelsList->Clear();
	hgeFont* mFont = new hgeFont("../Data/Fonts/Font2.fnt");
	mFont->SetColor(MY_FONT_COLOUR);

	if (XCourse::GetSingleton()->GetTopics()->size() == 0)
		return;

	TopicList* topics = XCourse::GetSingleton()->GetTopics();

	for (TopicList::iterator it = topics->begin(); it != topics->end(); ++it)
	{
		ListWidgetItem* item = new ListWidgetItem((*it)->name);
		item->SetFont(mFont);
		mLevelsList->AddItem(item);
	}
		
	mSlider->SetValue(0);
	mLevelsList->SetSelectedItem(mLevelsList->GetCount() - 1);
	mLevelsList->SetTopItem(0);
}

void PlayersMenuState::OnSliderMoved(void* params)
{
	Slider* slider = (Slider*)params;
	mLevelsList->Scroll(slider->GetRatioValue());
}

void PlayersMenuState::pause()
{
}
void PlayersMenuState::resume(){}
void PlayersMenuState::start() {}
void PlayersMenuState::OnUpdate(float elapsedTime)
{
	mUseSound->SetChecked(!(SoundManager::GetSingleton()->GetUseSound()));
}
bool PlayersMenuState::Draw(float elapsedTime)
{
	mLevel->Draw(elapsedTime);
	return true;
}

PlayersMenuState::~PlayersMenuState()
{
}

void PlayersMenuState::StartLevel(void* params)
{
	// ����� ����
	XCourse::GetSingleton()->StartTopic(mLevelsList->GetSelectedNumber());

	// ������ ����
	Engine::GetSingleton()->GetStateManager()->Push(mPlayState);
}

void PlayersMenuState::BackToMenu(void* params)
{
	XCourse::GetSingleton()->SaveProgress();
	Close();
}
void PlayersMenuState::SetPrisesSprites(void*)
{
	char text[20];
	itoa(XCourse::GetSingleton()->GetScore(), text, 10);
	mScoreView->SetText(text);
	GenerateLevelsList();
	if (XCourse::GetSingleton()->IsPriseOpened(PRISE_ONE_TOPIC))
		((SpriteModel*)mFirst->GetModel())->SetSprite("../Data/Textures/Awards.png", 72*0, 72, 72, 72);
	if (XCourse::GetSingleton()->IsPriseOpened(PRISE_HALF))
		((SpriteModel*)mHalf->GetModel())->SetSprite("../Data/Textures/Awards.png", 72*1, 72, 72, 72);
	if (XCourse::GetSingleton()->IsPriseOpened(PRISE_ALL))
		((SpriteModel*)mAll->GetModel())->SetSprite("../Data/Textures/Awards.png", 72*2, 72, 72, 72);
	if (XCourse::GetSingleton()->IsPriseOpened(PRISE_MORE_100))
		((SpriteModel*)mMore100->GetModel())->SetSprite("../Data/Textures/Awards.png", 72*3, 72, 72, 72);
	if (XCourse::GetSingleton()->IsPriseOpened(PRISE_MORE_300))
		((SpriteModel*)mMore300->GetModel())->SetSprite("../Data/Textures/Awards.png", 72*4, 72, 72, 72);
	if (XCourse::GetSingleton()->IsPriseOpened(PRISE_MORE_500))
		((SpriteModel*)mMore500->GetModel())->SetSprite("../Data/Textures/Awards.png", 72*5, 72, 72, 72);
}
void PlayersMenuState::OnStart()
{
	State::OnStart();
	mCurrentTopic = 0;
	SetPrisesSprites(0);
	mLevelsList->SetTopItem(0);
	mLevelsList->SetSelectedItem(0);
	char text[20];
	itoa(XCourse::GetSingleton()->GetScore(), text, 10);
	mScoreView->SetText(text);
}