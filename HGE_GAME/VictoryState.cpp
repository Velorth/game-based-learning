#include "Engine.h"
#include "resource.h"


void VictoryState::SetMessageText(std::string text)
{
	mText->SetText(text.data());
}
VictoryState::VictoryState()
{
	isTransporate = true;
	mPausePrev = true;

	mLevel = new Level();
	mMessageBox = new Entity();
	SpriteModel* boxModel = new SpriteModel("../Data/Textures/GameOverForm.png", 0, 0, 300, 200);
	HEFFECT sound = Engine::GetSingleton()->GetHGE()->Effect_Load("../Data/Sounds/CLICK.WAV");
	boxModel->SetSize(300, 200);
	mMessageBox->SetModel(boxModel);
	mMessageBox->Move(400, 150);
	mMessageBox->Rotate(-3.14 / 2);


	mCursor = new hgeSprite(Engine::GetSingleton()->GetResourceManager()->GetTexture("../Data/Textures/cursor.png"), 0, 0, 32, 32);

	mCancel = new PushButton();
	mOk = new PushButton();

	// mBackGround
	mBackground = new Layer();
	mBackground->Add(mMessageBox);

	// mOk
	mOk->SetClickSound(sound);
	mOk->SetPosition(280, 145, 95, 70);
	mOk->Click->Add(NewEventHandler(this, &VictoryState::OnOkClicked));
	mOk->SetHighlightedSprite("../Data/Textures/YesNo.png", 0, 70, 95, 70);
	mOk->SetSprite("../Data/Textures/YesNo.png", 0, 0, 95, 70);
	mOk->SetClickedSprite("../Data/Textures/YesNo.png", 0, 70, 95, 70);
	mOk->SetHotKey(HGEK_ENTER);

	mCancel->SetClickSound(sound);
	mCancel->SetPosition(440, 145, 85, 70);
	mCancel->Click->Add(NewEventHandler(this, &VictoryState::OnCancelClicked));
	mCancel->SetHighlightedSprite("../Data/Textures/YesNo.png", 95, 70, 85, 70);
	mCancel->SetSprite("../Data/Textures/YesNo.png", 95, 0, 85, 70);
	mCancel->SetClickedSprite("../Data/Textures/YesNo.png", 95, 70, 85, 70);
	mCancel->SetHotKey(HGEK_ESCAPE);

	// mText
	mText = new TextView();
	mText->SetPosition(400, 100, 200, 100);
	mText->SetMode(HGETEXT_CENTER);
	mText->SetFont("../Data/Fonts/font2.fnt");
	mText->GetFont()->SetColor(MY_FONT_COLOUR);

	mLevel->GetLayers()->push_back(mBackground);

	GetGUI()->SetCursor(mCursor);
	GetGUI()->AddCtrl(mCancel);
	GetGUI()->AddCtrl(mOk);
	GetGUI()->AddCtrl(mText);

	Done = new Event();
		
}

void VictoryState::OnCancelClicked(void* param)
{
	bool again = false;
	Close();
	Done->Invoke(&again);
}

void VictoryState::OnOkClicked(void* param)
{
	bool again = true;
	Close();
	Done->Invoke(&again);
}
void VictoryState::SetMessage(std::string text)
{
	mText->SetText(text);
}
void VictoryState::pause(){}
void VictoryState::resume(){}
void VictoryState::start(){}
void VictoryState::OnUpdate(float elapsedTime){}
bool VictoryState::Draw(float elapsedTime)
{
	mLevel->Draw(elapsedTime);
	return true;
}

VictoryState::~VictoryState()
{
}