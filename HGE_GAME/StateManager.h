#ifndef STATEMANAGER_H
#define STATEMANAGER_H

#include <stack>
#include <list>
#include <vector>
#include "State.h"

class StateManager
{
public:
	StateManager()
	{
	}
	bool Update(float elapsedTime);
	bool Draw(float elapsedTime);
	void Push(State* state)
	{
		mStates.push_back(state);
		state->OnStart();
	}

	void Pop()
	{
		mStates.pop_back();
	}

	virtual ~StateManager()
	{
	}

private:
	std::list <State*> mStates;
	std::list <State*> mUpdatedStates;
};

#endif