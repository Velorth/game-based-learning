#ifndef NEW_GUI_H
#define NEW_GUI_H

#include "GUIObject.h"
#include <hge.h>
#include <map>
#include "hgesprite.h"
#include "hgerect.h"
#include "IHotKey.h"


#define HGEGUI_NONAVKEYS		0
#define HGEGUI_LEFTRIGHT		1
#define HGEGUI_UPDOWN			2
#define HGEGUI_CYCLED			4

class GUI;

class GUIObject
{
public:
	GUIObject()	{ hge=hgeCreate(HGE_VERSION); color=0xFFFFFFFF; gui = 0;}
	virtual			~GUIObject() { hge->Release(); }

	virtual void	Render() = 0;
	virtual void	Update(float dt) {}

	virtual void	Enter() {}
	virtual void	Leave() {}
	virtual void	Reset() {}
	virtual bool	IsDone() { return true; }
	virtual void	Focus(bool bFocused) {}
	virtual void	MouseOver(bool bOver) {}

	virtual bool	MouseMove(float x, float y) { return false; }
	virtual bool	MouseLButton(bool bDown) { return false; }
	virtual bool	MouseRButton(bool bDown) { return false; }
	virtual bool	MouseWheel(int nNotches) { return false; }
	virtual bool	KeyClick(int key, int chr) { return false; }

	virtual void	SetColor(DWORD _color) { color=_color; }
	void SetGUI(GUI* g)
	{
		gui = g;
		OnSetGUI();
	}
	int				id;
	bool			bStatic;
	bool			bVisible;
	bool			bEnabled;
	hgeRect			rect;
	DWORD			color;

	GUI			*gui;
	GUIObject	*next;
	GUIObject	*prev;

protected:
	GUIObject(const GUIObject &go);
	GUIObject&	operator= (const GUIObject &go);

	static HGE		*hge;
	virtual void OnSetGUI()
	{
	}
};

class GUI
{
public:
	GUI();
	~GUI();

	void			AddCtrl(GUIObject *ctrl);
	void			DelCtrl(int id);
	GUIObject*	GetCtrl(int id) const;

	void			MoveCtrl(int id, float x, float y);
	void			ShowCtrl(int id, bool bVisible);
	void			EnableCtrl(int id, bool bEnabled);

	void			SetNavMode(int mode);
	void			SetCursor(hgeSprite *spr);
	void			SetColor(DWORD color);
	void			SetFocus(int id);
	int				GetFocus() const;
	
	void			Enter();
	void			Leave();
	void			Reset();
	void			Move(float dx, float dy);

	int				Update(float dt);
	void			Render();
private:
	GUI(const GUI &);
	GUI&			operator= (const GUI&);
	bool			ProcessCtrl(GUIObject *ctrl);

	static HGE		*hge;

	GUIObject	*ctrls;
	GUIObject	*ctrlLock;
	GUIObject	*ctrlFocus;
	GUIObject	*ctrlOver;

	int				navmode;
	int				nEnterLeave;
	hgeSprite		*sprCursor;

	float			mx,my;
	int				nWheel;
	bool			bLPressed, bLReleased;
	bool			bRPressed, bRReleased;
};

#endif